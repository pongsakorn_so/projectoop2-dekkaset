# DekKASET #

### Description ###
DekKASET is the program that help users remember the work,homework,event,etc. and remind them when that work is near to the deadline.


### Feature ###
* Subscribe the course.
* Remind users when there are work that near the deadline.
* Sort the priority and tell the users that what work must to do now.
* Show the calender and event of each day.
* Show homework of each course.
* Show course description.
* Show Schedule.
* Group Chat

### Team Member ###
* [Pongsakorn Somsri 5710546321](https://bitbucket.org/pongsakorn_so)
* [Parisa Supitayakul 5710546313](https://bitbucket.org/parisa_s)
* [Chinatip Vichian 5710546551](https://bitbucket.org/b5710546551)