package ChatServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * Server for all clients
 * 
 * @author Chinatip Vichian 5710546551
 *
 */
public class Server extends AbstractServer {
	private int state;
	final int LOGGEDIN = 0;
	final int LOGGEDOUT = 2;
	final int SENDING = 4;
	private CourseIDManager courseIDManager;

	/**
	 * Constructor of Server
	 * 
	 * @param port
	 *            is number of port
	 */
	public Server(int port) {
		super(port);
		courseIDManager = new CourseIDManager();
	}

	/** Connect Client to server **/
	protected synchronized void clientConnected(ConnectionToClient client) {
		client.setInfo("state", LOGGEDOUT);
	}

	/** Handle message that sent from Client **/
	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		String message = (String) msg;
		if (!(msg instanceof String)) {
			sendToClient(client, "Unrecognized Message");
			return;
		}

		state = (Integer) client.getInfo("state");
		switch (state) {
		case LOGGEDOUT:
			String sss = (String)msg;
			String arr[] = sss.split(" ");
			if (arr[0].equalsIgnoreCase("login")) {
				String courseID = arr[1];
				String name = arr[2];

				CourseChat courseChat = courseIDManager.getCourseChat(courseID);
				client.setInfo("username", name);

				courseChat.addClient(client);

				client.setInfo("state", LOGGEDIN);
				client.setInfo("coursechat", courseChat);

			} else {
				sendToClient(client, "Please Login");
				return;
			}
			break;

		case LOGGEDIN:
			if (message.equalsIgnoreCase("logout")) {
				sendToClient(client, "Goodbye");
				((CourseChat) client.getInfo("coursechat"))
				.removeClient(client);
				client.setInfo("state", LOGGEDOUT);
				clientDisconnected(client);
			} else if (message.startsWith("To: ")) {
				String name = message.substring(3).trim();
				ConnectionToClient toClient = ((CourseChat) client
						.getInfo("coursechat")).getClient(name);
				sendToClient(client, "Please type your message: ");
				client.setInfo("state", SENDING);
				client.setInfo("toClient", toClient);
			} else {
				if(message.length()>4){
					if(message.substring(0, 4).equals("%DK-")){
						((CourseChat) client.getInfo("coursechat"))
						.sendMessageToAllUser("--- "+client.getInfo("username")
								+ " : " + msg);
						break;
					}
				}
				((CourseChat) client.getInfo("coursechat"))
				.sendMessageToAllUser(client.getInfo("username")
						+ " : " + msg);
			}
			break;

		case SENDING:
			sendToClient((ConnectionToClient) client.getInfo("toClient"),
					message + " from " + client.getInfo("username"));
			client.setInfo("state", LOGGEDIN);
			break;
		}

	}

	/**
	 * Send message to individual client that received from parameter;
	 * 
	 * @param client
	 *            is specific client that will be sent.
	 * @param msg
	 *            is message from sender.
	 */
	private void sendToClient(ConnectionToClient client, Object msg) {
		try {
			client.sendToClient(msg);
		} catch (IOException e) {
		}

	}

	/**
	 * Close the connection of this Client from server.
	 * 
	 * @param client
	 */
	protected synchronized void clientDiscornnected(ConnectionToClient client) {
		((CourseChat) client.getInfo("coursechat")).getClientsList().remove(
				client);
	}

	/**
	 * Main method for opening server
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Server server = new Server(5555);
		System.out.println("Starting Server");
		try {
			server.listen();
		} catch (IOException e) {
			System.out.println("Fail to connect to Server");
		}

	}

}