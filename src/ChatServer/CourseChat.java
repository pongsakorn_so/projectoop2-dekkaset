package ChatServer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * CourseChat is like channel of chat divided by courseID from courseIDManager
 * 
 * @author Chinatip Vichian 5710546551
 *
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * CourseChat is like channel of chat divided by courseID from courseIDManager
 * 
 * @author Chinatip Vichian 5710546551
 *
 */
public class CourseChat {
	private List<ConnectionToClient> list;
	private String courseID;

	/** Constructor for CourseID **/
	public CourseChat(String courseID) {
		this.courseID = courseID;
		list = new ArrayList<ConnectionToClient>();
	}

	/**
	 * Add Client to list of clients in channel.
	 * 
	 * @param client
	 *            that will be add to list.
	 */
	public void addClient(ConnectionToClient client) {
		list.add(client);
		sendMessageToAllUser(client.getInfo("username") + " logged in");
		String allUsers ="@@@ ";
		for(ConnectionToClient c : list){
			allUsers += c.getInfo("username")+" ";
		}
		sendMessageToAllUser(allUsers);
	}

	/**
	 * Remove Client from list by client from parameter
	 * 
	 * @param client
	 *            that will be remove to list.
	 * 
	 */
	public void removeClient(ConnectionToClient client) {
		sendMessageToAllUser(client.getInfo("username") + " logged off");
		list.remove(client);
		String allUsers ="@@@ ";
		for(ConnectionToClient c : list){
			allUsers += c.getInfo("username")+" ";
		}
		sendMessageToAllUser(allUsers);
	}

	/**
	 * Get List of Clients in this channel
	 * 
	 * @return List of Clients in this channel
	 */
	public List<ConnectionToClient> getClientsList() {
		return list;
	}

	/**
	 * Send message to all Clients in this channel
	 * 
	 * @param msg
	 *            is message that wanted to be sent
	 * 
	 */
	public void sendMessageToAllUser(Object msg) {
		for (ConnectionToClient client : list) {
			try {
				client.sendToClient(msg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Get Client from list by username
	 * 
	 * @param username
	 *            is name of client in chanel
	 * @return Client that match username or Null.
	 */
	public ConnectionToClient getClient(String username) {
		for (ConnectionToClient c : list) {
			if (c.getInfo("username").equals(username))
				return c;
		}
		return null;
	}
}