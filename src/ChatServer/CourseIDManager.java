package ChatServer;
import java.util.HashMap;
import java.util.Map;

/**
 * CourseIDManager works like remote that switch channel of chatting by courseID
 * 
 * @author Chinatip Vichian 5710546551
 *
 */
public class CourseIDManager {
	private Map<String, CourseChat> map;

	/** Constructor for CourseIDManager **/
	public CourseIDManager() {
		map = new HashMap<String, CourseChat>();
	}

	/**
	 * Get channel of chat by courseIDS
	 * 
	 * @param courseID
	 * @return
	 */
	public CourseChat getCourseChat(String courseID) {
		if (map.containsKey(courseID)) {
			return map.get(courseID);
		}
		CourseChat courseChat = new CourseChat(courseID);
		map.put(courseID, courseChat);
		return courseChat;
	}

}