package ChatServer;
import java.util.Observable;

/**
 * Dispatcher is the sender class that extends Observable to make UI notice that
 * changes
 * 
 * @author Chinatip Vichian 5710546551
 *
 */
public class Dispatcher extends Observable {
	/**
	 * Notify Observers which is UI
	 * 
	 * @param msg
	 */
	public void notify(Object msg) {
		this.setChanged();
		this.notifyObservers(msg);
	}
}