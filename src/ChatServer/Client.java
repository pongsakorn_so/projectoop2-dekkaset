package ChatServer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;
import com.lloseng.ocsf.client.ObservableClient;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * Client that contains courseID and username for joining the chat
 * 
 * @author Chinatip Vichian 5710546551
 *
 */
public class Client extends AbstractClient {
	private String courseID, username;
	private Object msg;
	private Dispatcher dispatcher;
	private List<ConnectionToClient> onlineList;

	/** Constructor for Cient **/
	public Client(String host, int port, String courseID, String username)
			throws IOException {
		super(host, port);
		this.courseID = courseID;
		this.username = username;
		dispatcher = new Dispatcher();
		onlineList = new ArrayList<ConnectionToClient>();
	}

	/** Handle Message that want to send to server **/
	@Override
	protected void handleMessageFromServer(Object msg) {
		System.out.println("> " + msg);
		dispatcher.notify(msg);
	}

	/**
	 * Get Dispatcher of this Client
	 * 
	 * @return Dispatcher from this class.
	 */
	public Dispatcher getDispatcher() {
		return dispatcher;
	}

	/** Main method for Client **/
	/** Used to check server in case UI does not work **/
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		Client client = new Client("localhost", 5555, "12345678", "Suzan");
		// Client client = new Client("dekkaset.ddns.net", 5555,"12345678",
		// "Suzan");
		client.openConnection();
		client.sendToServer("Login 12345678 i");
		while (client.isConnected()) {
			String msg = in.nextLine().trim();
			if (msg.equalsIgnoreCase("quit")) {
				client.closeConnection();
				break;
			} else
				client.sendToServer(msg);
		}

	}

}