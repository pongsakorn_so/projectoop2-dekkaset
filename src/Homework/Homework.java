package Homework;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Homework for each course including courseID,name,description and deadline of work.
 * @author Pongsakorn Somsri.
 *
 */
public class Homework {
	private String courseID;
	private String homeworkName;
	private String description;
	private Date deadLine;
	
	/**
	 * Constructor for declare variable.
	 * @param courseID is id of each course.
	 * @param homeworkName is a title of homework.
	 * @param description is a description of homework.
	 * @param deadLine is a date for sending a work.
	 */
	public Homework(String courseID,String homeworkName,String description,String deadLine){
		this.courseID = courseID;
		this.homeworkName = homeworkName;
		this.description = description;
		
		String[] deadLineSplit = deadLine.split("/");
		
		this.deadLine = new Date(Integer.parseInt(deadLineSplit[2])-1900,Integer.parseInt(deadLineSplit[1])-1,Integer.parseInt(deadLineSplit[0]));
	}
	
	/**
	 * To get course ID.
	 * @return course ID.
	 */
	public String getCourseID(){
		return this.courseID;
	}
	
	/**
	 * To get title of homework.
	 * @return title of homework.
	 */
	public String getHomeworkName(){
		return this.homeworkName;
	}
	
	/**
	 * To get description of homework.
	 * @return description of homework.
	 */
	public String getDescription(){
		return this.description;
	}
	
	/**
	 * To get deadline of each homework.
	 * @return deadline of homework.
	 */
	public Date getDeadLine(){
		return this.deadLine;
	}
	
	/**
	 * To set homework title.
	 * @param name that used to set.
	 */
	public void setHomeworkName(String name){
		this.homeworkName =name;
	}
	
	/**
	 * To get remaining of day.
	 * @param currentDay that used to get remaining day.
	 * @return number of remaining day.
	 */
	public int getRemainingDay(int currentDay){
		return this.deadLine.getDate() - currentDay;
	}
	
	/**
	 * To get remaining of month.
	 * @param currentMonth that used to get remaining month.
	 * @return number of remaining month.
	 */
	public int getRemainingMonth(int currentMonth){
		return this.deadLine.getMonth() - currentMonth;
	}
	
	/**
	 * To get remaining of year.
	 * @param currentYear that used to get remaining year.
	 * @return number of remaining year.
	 */
	public int getRemainingYear(int currentYear){
		return this.deadLine.getYear() - currentYear;
	}
	
	/**
	 * To get String of deadline.
	 * @return Strin of deadline.
	 */
	public String deadLineToString(){
		return String.format("%d/%d/%d",this.getDeadLine().getDate(),this.getDeadLine().getMonth()+1,this.getDeadLine().getYear()+1900);
	}
	
	/**
	 * To get string.
	 */
	public String toString(){
		return String.format("Name:%s\nDescription:%s\nDeadline:%s", this.getHomeworkName(),this.getDescription(),this.getDeadLine().toLocaleString());
	}
}
