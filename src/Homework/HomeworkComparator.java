package Homework;
import java.util.Comparator;

/**
 * Comparator to compare each homework by deadline. 
 * @author Gateway
 *
 */
public class HomeworkComparator implements Comparator<Homework>{
	/**
	 * To compare homework. 
	 */
	@Override
	public int compare(Homework hw1, Homework hw2) {
		if(hw1.getDeadLine().getTime() > hw2.getDeadLine().getTime())
			return 1;
		
		if(hw1.getDeadLine().getTime() < hw2.getDeadLine().getTime())
			return -1;
			
		return 0;
	}

}
