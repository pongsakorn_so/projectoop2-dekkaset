package Homework;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * To manage homework.
 * @author Pongsakorn Somsri
 *
 */
public class HomeworkManager {
	//private List<Homework> homeworkList;
	/**
	 * Constructor 
	 */
	public HomeworkManager(){
		//this.homeworkList = homeworkList;
	}
	
	/*public List<Homework> getAllHomework(){
		return this.homeworkList;
	}*/
	
	/**
	 * To sort homework by using homeworkComparator.
	 * @param homeworkList that used for sort.
	 */
	public void getSortedAllHomework(List<Homework> homeworkList){
		Collections.sort(homeworkList, new HomeworkComparator());
	}
	
}