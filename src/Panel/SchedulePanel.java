package Panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import Course.Course;
import Course.CourseTime;
import Course.PeriodTime;
import MainApp.DekKASET;
import UI.CourseLabel;

/**
 * User interface that used to show the schedule of week.
 * @author Parisa Supitayakul
 *
 */
public class SchedulePanel extends JPanel implements Observer{

	private JPanel contentPane,schedulePanel,monPanel,tuePanel,wedPanel,thuPanel,friPanel,satPanel,sunPanel;
	private DekKASET dekKaset;
	private PeriodTime periodTime;
	private ClassLoader classLoader = this.getClass().getClassLoader();
	private ArrayList<CourseLabel> courseLabelList;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SchedulePanel frame = new SchedulePanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SchedulePanel() {
		dekKaset = DekKASET.getInstance();
		courseLabelList = new ArrayList<CourseLabel>();
		//super.setResizable(false);

		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 275);
		contentPane = this;
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		//setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLayout(null);
		this.setOpaque(false);

		JLabel lblSchedule = new JLabel("");
		lblSchedule.setIcon(new ImageIcon(SchedulePanel.class.getResource("/images/schedule.png")));
		lblSchedule.setHorizontalAlignment(SwingConstants.CENTER);
		lblSchedule.setBounds(1, 0, 400, 30);
		contentPane.add(lblSchedule);
		contentPane.setPreferredSize(new Dimension(380,270));



		schedulePanel = new JPanel();
		schedulePanel.setLayout(null);
		schedulePanel.setBounds(10,35,380,240);
		schedulePanel.setOpaque(false);
		contentPane.add(schedulePanel);

		JLabel lblScheduleTime = new JLabel();
		lblScheduleTime.setIcon(new ImageIcon(SchedulePanel.class.getResource("/images/ScheduleTime2.png")));
		lblScheduleTime.setBounds(10,35,380,240);

		this.handleNewData();
		super.add(lblScheduleTime);
		//schedulePanel.add(lblScheduleTime);

	}
	/**
	 * To update observer .
	 */
	public void update(Observable subject, Object info) {
		System.out.println("update");
		if(subject instanceof DekKASET){
			DekKASET dekKaset = (DekKASET) subject;
			handleNewData();
			repaint();
			if(info != null) {
				System.out.println(info);
			}
		}
	}
	/**
	 * To add new data.
	 */
	private void handleNewData() {
		for(CourseLabel label : courseLabelList){
			schedulePanel.remove(label);
		}
		
		int courseTime = 0;
		int keyTime = 0;
		int keyDay = 0;
		int color1 = 0;
		int color2 = 0;
		int color3 = 0;
		for(Course course : dekKaset.getEnrollList()){
			color1 = (int) Math.floor(Math.random()*255);
			color2 = (int) Math.floor(Math.random()*255);
			color3 = (int) Math.floor(Math.random()*255);
			for(CourseTime courseT: course.getCourseTimeList()){
				courseTime = courseT.getTimeDuration();
				for(PeriodTime time: PeriodTime.values()){
					//System.out.println(courseT.getStartTime()+"course time");
					//System.out.println(time.getTimePeriod()+"time Period");
					if(courseT.getStartTime().equals(time.getTimePeriod())){
						keyTime = time.getKeyPeriod()-1;
					}
				}
				keyDay = courseT.getDay().getDayInt()-1;
				if(keyDay>=7){
					keyDay = 1;
				}
				
				CourseLabel period = new CourseLabel(course);
				courseLabelList.add(period);
				period.setBackground(new Color(color1, color2, color3));
				period.setOpaque(true);
				period.setText(course.getCourseName());
				period.setBounds((keyTime*15)+20,(keyDay*30), courseTime*15,30);
				schedulePanel.add(period);				
			}
			//System.out.println(keyDay);
			System.out.printf("%d,%d,%d\n",courseTime,keyTime,keyDay);
		}
	}
}

