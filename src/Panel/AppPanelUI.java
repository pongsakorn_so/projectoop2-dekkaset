package Panel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Grade.GradeCalculator;
import Homework.HomeworkManager;
import UI.CalculateGradeUI;
import UI.CalendarUI;
import UI.CourseManagementUI;
import UI.CourseUI;
import UI.HomeworkUI;
import java.awt.Color;
import javax.swing.ImageIcon;
/**
 * User interface for show all applications of this program.
 * @author Parisa Supitayakul
 *
 */
public class AppPanelUI extends JPanel {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppPanelUI frame = new AppPanelUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AppPanelUI() {

		this.setPreferredSize(new Dimension(400, 120));
		this.setOpaque(false);
		contentPane = this;
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setLayout(null);

		JLabel lblApps = new JLabel("Apps");
		lblApps.setIcon(new ImageIcon(AppPanelUI.class.getResource("/images/appTab.png")));
		lblApps.setBackground(Color.GREEN);
		lblApps.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblApps.setBounds(1, 0, 400, 30);
		contentPane.add(lblApps);

		JButton btnCalGrade = new JButton("");
		btnCalGrade.setIcon(new ImageIcon(AppPanelUI.class.getResource("/images/calGrade.png")));
		btnCalGrade.setBounds(100, 45, 60, 60);
		contentPane.add(btnCalGrade);

		JButton btnCourseManage = new JButton("");
		btnCourseManage.setIcon(new ImageIcon(AppPanelUI.class.getResource("/images/courseManage.png")));
		btnCourseManage.setBounds(30, 45, 60, 60);
		contentPane.add(btnCourseManage);

		JButton btnCalendar = new JButton("");
		btnCalendar.setIcon(new ImageIcon(AppPanelUI.class.getResource("/images/calendar.png")));
		btnCalendar.setBounds(310, 45, 60, 60);
		contentPane.add(btnCalendar);

		JButton btnHomework = new JButton("");
		btnHomework.setIcon(new ImageIcon(AppPanelUI.class.getResource("/images/allHW.png")));
		btnHomework.setBounds(240, 45, 60, 60);
		add(btnHomework);
		
		JButton btnCourse = new JButton("");
		btnCourse.setIcon(new ImageIcon(AppPanelUI.class.getResource("/images/allcourse.png")));
		btnCourse.setBounds(170, 45, 60, 60);
		add(btnCourse);
		
		btnCourse.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				CourseUI ui = new CourseUI();
				ui.run();
			}

		});

		btnCalGrade.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				CalculateGradeUI ui = new CalculateGradeUI(new GradeCalculator());
				ui.run();
			}

		});

		btnCourseManage.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				CourseManagementUI ui = new CourseManagementUI();
				ui.run();
			}

		});

		btnHomework.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				HomeworkUI ui = new HomeworkUI(new HomeworkManager());
				ui.run();
			}

		});

		btnCalendar.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				CalendarUI ui = new CalendarUI();
				ui.run();
			}

		});
	}
}

