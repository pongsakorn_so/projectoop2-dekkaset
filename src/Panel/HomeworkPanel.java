package Panel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;

import java.awt.Scrollbar;

import javax.swing.JScrollPane;

import DigitalClock.SuperCheapClock;
import Homework.Homework;
import Homework.HomeworkManager;
import MainApp.DekKASET;
import UI.HomeworkLabel;

import java.awt.Font;
import javax.swing.ImageIcon;

/**
 * User interface to show homework in main menu. 
 * @author Pongsakorn Somsri
 *
 */
public class HomeworkPanel extends JPanel implements Observer{
	private DekKASET dekKaset;
	private SuperCheapClock clock;
	private JPanel homework;
	private HomeworkManager homeworkManager;
	private static int maxRow = 3;
	private List<HomeworkLabel> homeworkLabelList;

	/**
	 * Create the panel.
	 */
	public HomeworkPanel(HomeworkManager homeworkManager) {
		this.homeworkManager = homeworkManager;
		
		this.setPreferredSize(new Dimension(400, 172));
		this.setOpaque(false);
		
		setLayout(null);
		
		homeworkLabelList = new ArrayList<HomeworkLabel>();
		
		JLabel lblToDoList = new JLabel("");
		lblToDoList.setIcon(new ImageIcon(HomeworkPanel.class.getResource("/images/ToDoListTab.png")));
		lblToDoList.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblToDoList.setBounds(2, 0, 400, 30);
		add(lblToDoList);
		
		homework = new JPanel();
		homework.setOpaque(false);
		homework.setBounds(10, 35, 380, 120);
		add(homework);
		
		dekKaset = DekKASET.getInstance();
		clock = SuperCheapClock.getInstance();
		
		this.handleNewData();


	}
	/**
	 * To update homework.
	 */
	@Override
	public void update(Observable subject, Object info) {
	
		if(subject instanceof DekKASET){
			DekKASET dekKaset = (DekKASET) subject;
			System.out.println("update todolist");
			this.handleNewData();
			if(info != null) {
				System.out.println(info);
			}
		}
	}
	/**
	 * To reset data of homework panel.
	 */
	private void handleNewData(){
		for(HomeworkLabel label : this.homeworkLabelList){
			homework.remove(label);
		}
		this.repaint();
		List<Homework> homeworkList = dekKaset.getUserHomework();
		homeworkManager.getSortedAllHomework(homeworkList);
		int row = 0;
		for(int i =0;i<homeworkList.size();i++){
			Homework hw = homeworkList.get(i);
			if(row>=maxRow){
				break;
			}
			if(clock.getDate().before(hw.getDeadLine())){
				HomeworkLabel lb = new HomeworkLabel(hw);
				homeworkLabelList.add(lb);
				homework.add(lb);
				row++;
			}
		}
		this.validate();
	}

}
