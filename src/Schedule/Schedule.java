package Schedule;
import java.util.ArrayList;
import java.util.List;

import Course.Course;
import Course.CourseTime;
import Course.Day;

/**
 * A class that help manage courseTime in ScheduleUI.
 * @author Pongsakorn Somsri
 *
 */
public class Schedule {
	private List<Course> courseList;
	
	/**
	 * Constructor to declare variable
	 * @param courseList that used to manage.
	 */
	public Schedule(List<Course> courseList){
		this.courseList = courseList;
	}
	
	/**
	 * To get course list in each day.
	 * @param day that used to tell a day of week.
	 * @return course list . 
	 */
	public ArrayList<Course> getCourseArrList(Day day){
		ArrayList<Course> courseInThisDay = new ArrayList<Course>();
		for (Course c:courseList){
			for(CourseTime ct:c.getCourseTimeList()){
				if(ct.getDay().equals(day)){
					courseInThisDay.add(c);
				}
			}
		}
		return courseInThisDay;
		
	}
	
	/**
	 * To get course Time in each time.
	 * @param course ,to find the courstTime of each course.
	 * @param day that used to tell a day of week.
	 * @return courseTime of each course.
	 */
	public CourseTime getCourseTime(Course course,Day day){
		for(Course c:courseList){
			if(c.getCourseID().equals(course.getCourseID())){
				for(CourseTime ct:c.getCourseTimeList()){
					if(ct.getDay().equals(day))
						return ct;
				}
			}
		}
		return null;
	}
}