package Calendar;
import java.util.Date;

/**
 * Class of event in each day use to show the eventname description and date.
 * @author Pongsakorn Somsri
 *
 */
public class CalendarEvent {
	private String eventName;
	private String eventDescription;
	private Date eventDate;
	public CalendarEvent(String eventName,String eventDescription,String eventDate){
		this.eventName = eventName;
		this.eventDescription = eventDescription; 
		
		String[] eventDateSplit = eventDate.split("/");
		
		this.eventDate = new Date(Integer.parseInt(eventDateSplit[2])-1900,Integer.parseInt(eventDateSplit[1])-1,Integer.parseInt(eventDateSplit[0]));
	}
	
	public String getEventName(){
		return this.eventName;
	}
	
	public String getEventDescription(){
		return this.eventDescription;
	}
	
	public Date getEventDate(){
		return this.eventDate;
	}
	
	/**
	 * get the remaining day by given time - eventtime.
	 * @param currentDay day use to compare and get remaining day.
	 * @return remaining day
	 */
	public int getRemainingDay(int currentDay){
		return this.eventDate.getDate() - currentDay;
	}
	
	/**
	 * get the remaining month by given time - eventtime.
	 * @param currentDay month use to compare and get remaining day.
	 * @return remaining month
	 */
	public int getRemainingMonth(int currentMonth){
		return this.eventDate.getMonth()+1 - currentMonth;
	}
	
	/**
	 * get the remaining year by given time - eventtime.
	 * @param currentDay year use to compare and get remaining day.
	 * @return remaining year
	 */
	public int getRemainingYear(int currentYear){
		return this.eventDate.getYear()+1900 - currentYear;
	}
	
	public boolean isArrive(int curDay,int curMonth,int curYear){
		if(this.getRemainingDay(curDay)==0&&this.getRemainingMonth(curMonth)==0&&this.getRemainingYear(curYear)==0)
			return true;
		
		return false;
	}
	
	public String deadLineToString(){
		return String.format("%d/%d/%d",eventDate.getDate(),eventDate.getMonth()+1,eventDate.getYear()+1900);
	}
	
}
