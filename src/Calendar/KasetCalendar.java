package Calendar;
import java.util.ArrayList;
import java.util.List;

import Data.KasetLibrary;

/**
 * Calendar activity of Kasetsart University. 
 * @author Pongsakorn Somsri
 *
 */
public class KasetCalendar {
	KasetLibrary library;
	
	/**
	 * Constructor for declare variable.
	 */
	public KasetCalendar(){
		library = KasetLibrary.getInstance();
	}
	
	/**
	 * To get all calendar event.
	 * @return list of event.
	 */
	public List<CalendarEvent> getAllCalendarEvent(){
		return library.getCalendarList();
	}
	
	/**
	 * To get event per month .
	 * @param month that used to get event.
	 * @return list of event in that month.
	 */
	public List<CalendarEvent> getMonthEvent(int month){
		List<CalendarEvent> thisMonthEvent = new ArrayList<CalendarEvent>();
		for(CalendarEvent event : library.getCalendarList()){
			if(event.getEventDate().getMonth() == month){
				thisMonthEvent.add(event);
			}
		}
		return thisMonthEvent;
	}
	/**
	 * To get event per day .
	 * @param day that used to get event.
	 * @return list of event in that day.
	 */
	public List<CalendarEvent> getDayEvent(int day){
		List<CalendarEvent> thisDayEvent = new ArrayList<CalendarEvent>();
		for(CalendarEvent event : library.getCalendarList()){
			if(event.getEventDate().getDate() == day){
				thisDayEvent.add(event);
			}
		}
		return thisDayEvent;
	}
}
