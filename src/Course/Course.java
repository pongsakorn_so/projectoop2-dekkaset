package Course;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Homework.Homework;

/**
 * A course that for student to enroll in university.
 * @author Parisa Supitayakul.
 *
 */
public class Course {
	private String courseID;
	private String courseName;
	private String courseDescription;
	private int courseCredit;

	private List<CourseTime> courseTimeList;
	private List<Homework> homeworkList;

	/**
	 * Constructor for declare variable.
	 * @param courseCode is a ID of each course.
	 * @param courseName is a title of course.
	 * @param courseDescription for explain about course.
	 * @param courseCredit is a weight of course.
	 */
	public Course(String courseCode,String courseName,String courseDescription,int courseCredit){
		this.courseID = courseCode;
		this.courseName = courseName;
		this.courseDescription = courseDescription;
		this.courseCredit = courseCredit;
		courseTimeList = new ArrayList<CourseTime>();
		homeworkList = new ArrayList<Homework>();
	}
	
	public String getCourseID(){
		return this.courseID;
	}

	public String getCourseName(){
		return this.courseName;
	}

	public String getCourseDescription(){
		return this.courseDescription;
	}

	public int getCourseCredit(){
		return this.courseCredit;
	}
	
	public List<CourseTime> getCourseTimeList(){
		return this.courseTimeList;
	}
	
	public List<Homework> getHomeworkList(){
		return homeworkList;
	}
	/**
	 * To check course is equals to another course with course ID.
	 * @param obj is object that used for check.
	 * @return true if course is equals , false if course is not equals.
	 */
	public boolean equal(Object obj){
		if(obj==null)
			return false;
		
		if(this.getClass()!=obj.getClass())
			return false;
		
		Course other = (Course)obj;
		
		return this.courseID.equals(other.getCourseID());
	}
	
	/**
	 * To add course time for course.
	 * @param day is a day that course registed.
	 * @param startTime is time that start learning.
	 * @param endTime is time that end of learning.
	 */
	public void addCourseTime(Day day,String startTime,String endTime){
		CourseTime newCourseTime = new CourseTime(this.courseID,day,startTime,endTime);
		courseTimeList.add(newCourseTime);
	}
	
	/**
	 * To add homework of this course that was add by admin(Teacher)
	 * @param name is a title of homework.
	 * @param description to explain about homework.
	 * @param deadLine is time that tell for sending homework.
	 */
	public void addHomework(String name,String description,String deadLine){
		Homework newHomework = new Homework(this.courseID,name,description,deadLine);
		homeworkList.add(newHomework);
	}

	/**
	 * To remove homework from list of homework.
	 * @param homework that used to remove.
	 * @return true if it can remove , false if it can't.
	 */
	public boolean removeHomework(Homework homework){
		
		return homeworkList.remove(homework);
	}
	
	/**
	 * To print all homework.
	 */
	public void printAllHomework(){
		for(Homework homework : homeworkList){
			System.out.println(homework.toString());
		}
	}
	
	/**
	 * To print all course time per course.
	 */
	public void printAllCourseTime(){
		for(CourseTime courseTime : courseTimeList){
			System.out.println(courseTime.toString());
		}
	}
	
	/**
	 * To get String about course.
	 */
	public String toString(){
		return String.format("%s\n%s\n%s\n%d\n", this.courseID,this.courseName,this.courseDescription,this.courseCredit);
	}
}
