package Course;
/**
 * Enum of period time every 30 minutes .
 * @author Parisa Supitayakul
 *
 */
public enum PeriodTime {
	PERIOD_ONE(1,"08:00"),
	PERIOD_TWO(2,"08:30"),
	PERIOD_THREE(3,"09:00"),
	PERIOD_FOUR(4,"09:30"),
	PERIOD_FIVE(5,"10:00"),
	PERIOD_SIX(6,"10:30"),
	PERIOD_SEVEN(7,"11:00"),
	PERIOD_EIGHT(8,"11:30"),
	PERIOD_NINE(9,"12:00"),
	PERIOD_TEN(10,"12:30"),
	PERIOD_TONE(11,"13:00"),
	PERIOD_TTWO(12,"13:30"),
	PERIOD_TTHREE(13,"14:00"),
	PERIOD_TFOUR(14,"14:30"),
	PERIOD_TFIVE(15,"15:00"),
	PERIOD_TSIX(16,"15:30"),
	PERIOD_TSEVEN(17,"16:00"),
	PERIOD_TEIGHT(18,"16:30"),
	PERIOD_TNINE(19,"17:00"),
	PERIOD_TWEENTY(20,"17:30"),
	PERIOD_TWONE(21,"18:00"),
	PERIOD_TWTWO(22,"18:30"),
	PERIOD_TWTHREE(23,"19:00"),
	PERIOD_TWFOUR(24,"19:30");
	
	int keyPeriod ;
	String timePeriod;
	
	/**
	 * Constructor for declare variable.
	 * @param key is int of each period.
	 * @param time is string of start time.
	 */
	PeriodTime(int key,String time){
		this.keyPeriod = key;
		this.timePeriod = time;
	}
	
	/**
	 * To get int of each period.
	 * @return keyPeriod.
	 */
	public int getKeyPeriod(){
		return keyPeriod;
	}
	/**
	 * To get String of start time.
	 * @return String of strat time.
	 */
	public String getTimePeriod(){
		return timePeriod;
	}
	
	/**
	 * To get String of start time .
	 */
	public String toString(){
		return this.timePeriod;
	}
	
}