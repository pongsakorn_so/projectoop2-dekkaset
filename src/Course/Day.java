package Course;
/**
 * Enum of day include day of the week with the specific dayInt value.
 * @author Chinatip Vichien.
 *
 */
public enum Day {
	SUNDAY("Sun",1),
    MONDAY("Mon",2),
    TUESDAY("Tue",3),
    WEDNESDAY("Wed",4),
    THURSDAY("Thu",5),
    FRIDAY("Fri",6),
    SATURDAY("Sat",7);
    
    
    public String dayName;
    public int dayInt;
    Day(String dayName,int dayInt){
        this.dayName = dayName;
        this.dayInt = dayInt;
    }

    public String getDayName(){
        return dayName;
    }
    public int getDayInt(){
        return dayInt;
    }
    public String toString(){
    	return this.dayName;
    }
    
}
