package Course;
import java.sql.Time;
/**
 * Class that use to show the course time of each course.
 * @author Chinatip Vichian
 *
 */
public class CourseTime {
	private String subjectCode;
    private Day day;
    private Time startTime,endTime;
    private int hourDiff,minDiff;
    public CourseTime(String subjectCode,Day day,String startTime,String endTime){
    	this.subjectCode = subjectCode;
        this.day = day;
        
        String[] startTimeSplit = startTime.split(":");
        String[] endTimeSplit = endTime.split(":");
        
        this.startTime = new Time(Integer.parseInt(startTimeSplit[0]),Integer.parseInt(startTimeSplit[1]),Integer.parseInt(startTimeSplit[2]));
        this.endTime = new Time(Integer.parseInt(endTimeSplit[0]),Integer.parseInt(endTimeSplit[1]),Integer.parseInt(endTimeSplit[2]));;
    }
    public String getSubjectCode(){
    	return subjectCode;
    }
    public String getStartTime(){
        return String.format("%02d:%02d",startTime.getHours(),startTime.getMinutes());
    }
    public String getEndTime(){
    	return String.format("%02d:%02d",endTime.getHours(),endTime.getMinutes());
    }
    /**
     * get the get the enum day of this.
     * @return enum day.
     */
    public Day getDay(){
        return day;
    }
    
    /**
     * Method use to get time duration between two time.
     * @return new Time that was between two time.
     */
    public Time getDuration(){
            hourDiff = (int)java.time.Duration.between(startTime.toLocalTime(), endTime.toLocalTime()).toHours();
            minDiff = (int)java.time.Duration.between(startTime.toLocalTime(), endTime.toLocalTime()).toMinutes() - (hourDiff*60);
        return new Time(hourDiff, minDiff, 00);
    }
    
    /**
     * Method user to get time duration in /30 format.
     * @return
     */
    public int getTimeDuration(){
    	int hour = this.endTime.getHours()-this.startTime.getHours();
    	int minute = this.endTime.getMinutes()-this.startTime.getMinutes();
    	
    	return (hour*60+minute)/30;
    }
    
    public String toString(){
    	return String.format("%s %s-%s",this.getDay().toString(),this.getStartTime(),this.getEndTime());
    }
}
