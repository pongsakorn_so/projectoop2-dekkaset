package Clock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
/**
 * Cheap Clock UI that can set time of alarm clock
 * @author Chinatip Vichian
 *
 */
public class CheapClock extends JLabel implements Observer{
	/** Clock that will run and change state according to the response of UI **/
	private Clock clock;
	/** Container containing all components **/
	private Container container;
	/** number that counted to blink the selected set time ex. hour, minute ,second  **/
	private int blinkCount =0;
	/** ClassLoader that help contains images and sound **/
	private ClassLoader classLoader;
	/** Font for button **/
	private Font fontButton;
	/** Label in this UI **/
	private JLabel on,off,hour1,hour2,min1,min2,sec1,sec2;
	/** Array of ImageIcon for setting time images in JLabel **/
	private ImageIcon[] picArr;
	/** Beep sound for alarm clock **/
	private AudioInputStream beep;
	/** Clip helps run beep sound **/
	public static Clip clip;
	public CheapClock(Clock clock){
		super("Cheap Clock");
		this.clock = clock;
		classLoader = this.getClass().getClassLoader();
		picArr = new ImageIcon[12];
		this.initComponents();
		this.setVisible(true);
		this.setSize(250, 180);

	}
	/** Components of Cheap Clock**/
	public void initComponents(){
		this.setPreferredSize(new Dimension(400,100));
		super.setBorder(new EmptyBorder(0,0,0,0));
		
		
		container = this;
		container.setLayout(new BoxLayout(container,BoxLayout.Y_AXIS));
		container.setBackground(Color.black);
		JPanel pane0 = new JPanel();
		pane0.setLayout(new BorderLayout());

		JPanel pane1 = new JPanel();
		pane1.setLayout(new FlowLayout());

		for(int i=0;i<10;i++){
			picArr[i] = new ImageIcon(classLoader.getResource("images/"+i+"s.gif"));
		}

		picArr[10] = new ImageIcon(classLoader.getResource("images/colon_s.gif"));
		picArr[11] = new ImageIcon(classLoader.getResource("images/bs.gif"));


		hour1= new JLabel();
		min1 = new JLabel();
		sec1 = new JLabel();
		hour2= new JLabel();
		min2 = new JLabel();
		sec2 = new JLabel();
		JLabel s1 = new JLabel();
		JLabel s2 = new JLabel();
		s1.setIcon(picArr[10]);
		s2.setIcon(picArr[10]);
		fontButton = new Font("Tahoma", Font.BOLD, 15);


		pane1.add(hour1);
		pane1.add(hour2);
		pane1.add(s1);
		pane1.add(min1);
		pane1.add(min2);
		pane1.add(s2);
		pane1.add(sec1);
		pane1.add(sec2);

		JPanel pane2 = new JPanel();
		pane2.setLayout(new FlowLayout());
		JButton setB = new JButton("  SET  ");
		JButton plusB = new JButton("  +  ");
		JButton minusB = new JButton("  -  ");

		setB.setFont(fontButton);
		plusB.setFont(fontButton);
		minusB.setFont(fontButton);

		setB.setBackground(Color.BLACK);
		plusB.setBackground(Color.BLACK);
		minusB.setBackground(Color.BLACK);

		setB.setForeground(Color.WHITE);
		plusB.setForeground(Color.WHITE);
		minusB.setForeground(Color.WHITE);

		setB.addActionListener(new setButtonListener());
		plusB.addActionListener(new plusButtonListener());
		minusB.addActionListener(new minusButtonListener());
		pane2.add(setB);
		pane2.add(plusB);
		pane2.add(minusB);

		on = new JLabel("  ON");
		off = new JLabel("  OFF");
		on.setForeground(Color.green);
		off.setForeground(Color.red);
		on.setVisible(false);
		pane0.add(on,BorderLayout.WEST);
		pane0.add(off,BorderLayout.NORTH);

		pane0.setBackground(null);
		pane1.setBackground(null);
		pane2.setBackground(null);


		container.add(pane0);
		container.add(pane1);
		container.add(pane2);
	}
	/** ActionListener for set button **/
	class setButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) { 
			clock.handleSetKey();
		}

	}
	/** ActionListener for plus button **/
	class plusButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			clock.state.plus();
		}

	}
	/** ActionListener for plus button **/
	class minusButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			clock.state.minus();
		}

	}
	/** Update from Observable which is Clock **/
	@Override
	public void update(Observable arg0, Object arg1) {

		if(clock.hour == clock.ringHour && clock.min == clock.ringMin && clock.sec == clock.ringSec && clock.state instanceof DisplayTimeState){
			clock.setRingState();

			try {
				beep = AudioSystem.getAudioInputStream(classLoader.getResource("sound/beep.wav"));
				clip = AudioSystem.getClip();
				clip.open(beep);
				
				clip.loop(Clip.LOOP_CONTINUOUSLY);
			} catch (UnsupportedAudioFileException e) {

			} catch (IOException e) {
			}
			catch (LineUnavailableException e) {
			}
			clip.start();
			
		}
		else{
			if(clock.alarmOn){
				on.setVisible(true);
				off.setVisible(false);
			}
			else{
				on.setVisible(false);
				off.setVisible(true);
			}

		}
		blink(clock.blink());
		blinkCount++;
	}

	/** Blink for SetHourState, SetMinuteState, SetSecondState state**/
	public void blink(int i){
		if(clock.blink()<3 && blinkCount%2==0){
			if(i==0){
				hour1.setIcon(picArr[clock.hour/10]);
				hour2.setIcon(picArr[clock.hour%10]);
			}
			else if(i==1){
				min1.setIcon(picArr[clock.min/10]);
				min2.setIcon(picArr[clock.min%10]);
			}
			else{
				sec1.setIcon(picArr[clock.sec/10]);
				sec2.setIcon(picArr[clock.sec%10]);
			}
		}
		else if(clock.blink()<3){
			if(i==0){
				hour1.setIcon(picArr[11]);
				hour2.setIcon(picArr[11]);
				min1.setIcon(picArr[clock.min/10]);
				min2.setIcon(picArr[clock.min%10]);
				sec1.setIcon(picArr[clock.sec/10]);
				sec2.setIcon(picArr[clock.sec%10]);

			}
			else if(i==1){
				min1.setIcon(picArr[11]);
				min2.setIcon(picArr[11]);
				hour1.setIcon(picArr[clock.hour/10]);
				hour2.setIcon(picArr[clock.hour%10]);
				sec1.setIcon(picArr[clock.sec/10]);
				sec2.setIcon(picArr[clock.sec%10]);
			}
			else{
				sec1.setIcon(picArr[11]);
				sec2.setIcon(picArr[11]);
				hour1.setIcon(picArr[clock.hour/10]);
				hour2.setIcon(picArr[clock.hour%10]);
				min1.setIcon(picArr[clock.min/10]);
				min2.setIcon(picArr[clock.min%10]);
			}
		}
		else{
			hour1.setIcon(picArr[clock.hour/10]);
			hour2.setIcon(picArr[clock.hour%10]);
			min1.setIcon(picArr[clock.min/10]);
			min2.setIcon(picArr[clock.min%10]);
			sec1.setIcon(picArr[clock.sec/10]);
			sec2.setIcon(picArr[clock.sec%10]);
		}
		if(clock.state instanceof RingingState){
			if(blinkCount%2==0){
				hour1.setIcon(picArr[clock.hour/10]);
				hour2.setIcon(picArr[clock.hour%10]);
				min1.setIcon(picArr[clock.min/10]);
				min2.setIcon(picArr[clock.min%10]);
				sec1.setIcon(picArr[clock.sec/10]);
				sec2.setIcon(picArr[clock.sec%10]);
			}
			else{
				hour1.setIcon(picArr[11]);
				hour2.setIcon(picArr[11]);
				min1.setIcon(picArr[11]);
				min2.setIcon(picArr[11]);
				sec1.setIcon(picArr[11]);
				sec2.setIcon(picArr[11]);
			}
		}
	}
}
