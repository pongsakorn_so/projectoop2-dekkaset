package Clock;
/**
 * SetMinuteState is the state that CheapClock is during setting alarm time for minute.
 * @author Chinatip Vichian
 *
 */public class SetMinuteState extends ClockState{

	public SetMinuteState(Clock clock) {
		super(clock);

	}
	/** handle "set" key press */
	@Override
	public void performSet() {
		clock.state = new SetSecondState(clock);
	}
	/** handle updateTime event notification */
	@Override
	public void updateTime() {
		
	}
	/** Call this method when plus button is pressed **/
	@Override
	public void plus() {
		if(clock.min>=0 && clock.min<59) clock.min++;
		else clock.min = 0;
		clock.updateTime();
	}
	/** Call this method when minus button is pressed **/
	@Override
	public void minus() {
		if(clock.min>0) clock.min--;
		else clock.min = 59;
		clock.updateTime();
	}
	

}
