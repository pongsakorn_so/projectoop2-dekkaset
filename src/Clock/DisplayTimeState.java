package Clock;

import java.util.Date;
/**
 * DisplayTimeState is state that will show time that updates every second
 * @author Chinatip Vichian
 *
 */
public class DisplayTimeState extends ClockState{
	/** Date used for get current time **/
	static Date time = new Date();
	public DisplayTimeState(Clock clock) {
		super(clock);
		time.setTime(System.currentTimeMillis());
	}
	/** handle "set" key press */
	@Override
	public void performSet() {
		clock.state = new SetHourState(clock);
	}
	/** handle updateTime event notification */
	@Override
	public void updateTime() {
		time.setTime(System.currentTimeMillis());
		clock.hour = time.getHours();
		clock.min = time.getMinutes();
		clock.sec = time.getSeconds();
	}
	/** Call this method when plus button is pressed **/
	@Override
	public void plus() {
		clock.state = new AlarmTimeState(clock);
	}
	/** Call this method when minus button is pressed **/
	@Override
	public void minus() {
	}




}
