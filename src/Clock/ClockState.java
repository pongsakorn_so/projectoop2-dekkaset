package Clock;
/**
 * ClockState which is super class for state help run clock.
 * @author Chinatip Vichian
 *
 */
public abstract class ClockState {
	/** Clock received from parameter **/
	protected Clock clock; 
	
	/** Constructor of ClockState **/
	public ClockState(Clock clock) { this.clock = clock; }
	
	/** handle "set" key press */
	public abstract void performSet( ) ;
	
	/** handle updateTime event notification */
	public abstract void updateTime();

	/** something to do before exiting this state */
	public void leaveState( ) { };
	
	/** Call this method when plus button is pressed **/
	public abstract void plus();
	
	/** Call this method when minus button is pressed **/
	public abstract void minus();
}