package Clock;

import java.util.TimerTask;
/**
 * ClockTask help updating Clock 
 * @author Chinatip Vichian
 *
 */
public class ClockTask extends TimerTask{
	/** Clock that received from parameter **/
	private Clock clock;
	
	/** Constructor of ClockTask **/
	public ClockTask( Clock clock ){
		this.clock = clock;
	}
	/** Run to update task **/
	@Override
	public void run() {
		clock.updateTime();
	}

}
