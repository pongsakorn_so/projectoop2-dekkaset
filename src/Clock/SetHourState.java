package Clock;
/**
 * SetHourState is the state that CheapClock is during setting alarm time for hour.
 * @author Chinatip Vichian
 *
 */
public class SetHourState extends ClockState{
	public SetHourState(Clock clock) {
		super(clock);
	}
	/** handle "set" key press */
	@Override
	public void performSet() {
		clock.state = new SetMinuteState(clock);
	}
	/** handle updateTime event notification */
	@Override
	public void updateTime() {
		
	}
	/** Call this method when plus button is pressed **/
	@Override
	public void plus() {
		if(clock.hour>=0&&clock.hour<23) clock.hour++;
		else clock.hour = 0;
		clock.updateTime();
	}
	/** Call this method when minus button is pressed **/
	@Override
	public void minus() {
		if(clock.hour>0) clock.hour--;
		else clock.hour = 23;
		clock.updateTime();
	}
	

}
