package User;
import java.util.ArrayList;
import java.util.List;

/**
 * A class of member that used this program.
 * @author Parisa Supitayakul 
 *
 */
public class User {
	private String userID;
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String email;
	private List<String> enrollList;

	
	public User(String ID, String username,String password,String firstname,String lastname, String email){
		this.userID = ID;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;

		enrollList = new ArrayList<String>();

	}
	
	public String getUserID(){
		return userID;
	}
	public String getUsername(){
		return username;
	}
	public String getPassword(){
		return password;
	}
	public String getFirstname(){
		return firstname;
	}
	public String getLastname(){
		return lastname;
	}
	public String getEmail(){
		return email;
	}
	public List<String> getEnrollList(){
		return enrollList;
	}
	
	/**
	 * To check user that equals with another user. 
	 */
	public boolean equals(Object obj){
		if(obj == null ){
			return false;
		}
		if(this.getClass() != obj.getClass()){
			return false;
		}
		User student = (User) obj;
		if(this.getUserID().equals(student.getUserID())){
			return true;
		}
		return false;
	}
	
	/**
	 * To check it is an email or not
	 * @param emails that used for check.
	 * @return true if it's email , false if it isn't.
	 */
	public boolean isEmail(String emails){
		char[] emailCharlist = emails.toCharArray();
		for(char email : emailCharlist){
			if(email == '@'){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * To print enroll list of user.
	 */
	public void printEnrollList(){
		String tmp = "";
		for(String course: enrollList) {
			tmp += course.toString()+"\n";
		}
		System.out.print(tmp);

	}
	
	/**
	 * To get string of this user.
	 */
	public String toString(){
		return String.format("%s, %s, %s, %s\n", this.userID,this.firstname,this.lastname,this.enrollList);
	}
	
	/**
	 * To check library has this course or not.
	 * @param courseID that used for check.
	 * @return true if it already in library. false when it doesn't have.
	 */
	public boolean hasThisCourse(String courseID){
		
		for(String subs: enrollList){
			if(subs.equalsIgnoreCase(courseID)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * If it doesn't have this course ,then add course to enrollList.
	 * @param courseID that used to check.
	 * @return true if it can add,false when it alreadyhave this course. 
	 */
	public boolean addCourse(String courseID){
		if(!hasThisCourse(courseID)){
			enrollList.add(courseID);
			return true;
		}
		return false;
	}
	
	public boolean removeCourse(String courseID){
		if(hasThisCourse(courseID)){
				enrollList.remove(courseID);
				return true;
		}
		return false;
	}
	
	public void clearCourse(){
		for(String subs : enrollList){
			enrollList.remove(subs);
		}
	}
}