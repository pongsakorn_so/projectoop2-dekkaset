package MainApp;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import User.User;
import Calendar.CalendarEvent;
import Course.Course;
import Course.CourseTime;
import Data.Database;
import Data.KasetLibrary;
import Homework.Homework;

/**
 * Facade pattern that mix many class, method ,operation into an easy method.
 * @author Pongsakorn Somsri
 *
 */
public class DekKASET extends Observable{
	private static DekKASET dekKASET;
	private KasetLibrary library;
	private User user;
	private Database database;
	
	/**
	 * Constructor for declare variable.
	 */
	private DekKASET(){
		library = KasetLibrary.getInstance();
		database = Database.getInstance();
	}
	
	/**
	 * To get a singleTon dekKaset.
	 * @return object dekKaset.
	 */
	public static DekKASET getInstance(){
		if(dekKASET==null)
			dekKASET = new DekKASET();
		
		return dekKASET;	
	}
	
	/**
	 * To get user.
	 * @return user.
	 */
	public User getUser(){
		return this.user;
	}
	
	/**
	 * To get all course from library.
	 * @return all course from library.
	 */
	public List<Course> getAllCourse(){
		return library.getAllCourse();
	}

	/**
	 * To get calendar event in calendar.
	 * @return calendar list from library.
	 */
	public List<CalendarEvent> getCalendarEvent(){
		return library.getCalendarList();
	}

	/**
	 * To get enroll list of each user.
	 * @return enroll list of user.
	 */
	public List<Course> getEnrollList(){
		List<Course> enrollList = new ArrayList<Course>();
		for(String courseID : user.getEnrollList()){
			if(library.hasThisCourse(courseID)){
				enrollList.add(library.getCourse(courseID));
			}
		}
		return enrollList;
	}
	
	/**
	 * To get courseTime of user.
	 * @return courseTime of user.
	 */
	public List<CourseTime> getAllCourseTime(){
		List<Course> enrollCourse = this.getEnrollList();
		List<CourseTime> allCourseTime = new ArrayList<CourseTime>();
		for(Course course : enrollCourse){
			for(CourseTime courseTime : course.getCourseTimeList()){
				allCourseTime.add(courseTime);
			}	
		}
		
		return allCourseTime;
	}

	/**
	 * To get homework of user.
	 * @return homework of user.
	 */
	public List<Homework> getUserHomework(){
		List<Course> enrollList = this.getEnrollList();
		List<Homework> allHomeworkList = new ArrayList<Homework>();
		for(Course course : enrollList){
			List<Homework> courseHomeworkList = course.getHomeworkList();

			for(Homework homework : courseHomeworkList)
				allHomeworkList.add(homework);

		}
		return allHomeworkList;
	}

	/**
	 * To add course into the user with courseID
	 * @param courseID that used to add to user.
	 * @return true if it successful ,false if it isn't.
	 */
	public boolean addCourseToUser(String courseID){
		if(!library.hasThisCourse(courseID)){
			return false;
		}

		if(user.hasThisCourse(courseID)){
			return false;
		}

		database.addEnrollToUserInDB(user.getUserID(), courseID);
		user.addCourse(courseID);
		super.setChanged();
		super.notifyObservers();
		
		return true;
	}
	
	/**
	 * To remove course from user with courseID.
	 * @param courseID that used for remove.
	 * @return true if it's successful, false if it isn't.
	 */
	public boolean removeCourseFromUser(String courseID){
		if(!library.hasThisCourse(courseID)){
			return false;
		}

		if(!user.hasThisCourse(courseID)){
			return false;
		}

		database.removeEnrollFromUserInDB(user.getUserID(), courseID);
		user.removeCourse(courseID);
		super.setChanged();
		super.notifyObservers();
		
		return true;
	}
	
	/**
	 * To get course by use courseID.
	 * @param courseID that used for get Course.
	 * @return true if library can get this course.
	 */
	public Course getCourse(String courseID){
		return library.getCourse(courseID);
	}
	
	/**
	 * To add new course to the library.
	 * @param course that used to add into library.
	 * @return true if it can add,false if it can't.
	 */
	public boolean addNewCourseToLibraly(Course course){
		if(library.hasThisCourse(course.getCourseID()))
			return false;

		library.addCourseToDB(course);
		return true;
	}
	
	/**
	 * To set user 
	 * @param user that used to set.
	 */
	public void setUser(User user){
		this.user = user;
	}
	
	/**
	 * To print  all enroll course.
	 */
	public void printEnrollCourse(){
		for(Course homework : this.getEnrollList()){
			System.out.println(homework.toString());
		}
	}
	
	public void printAllCourse(){
		for(Course homework : this.getAllCourse()){
			System.out.println(homework.toString());
		}
	}

	public void printAllHomework(){
		for(Homework homework : this.getUserHomework()){
			System.out.println(homework.toString());
		}
	}
	
	



}
