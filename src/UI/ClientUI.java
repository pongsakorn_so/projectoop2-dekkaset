package UI;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import ChatServer.Client;

/**
 * UI of Client
 * 
 * @author Chinatip Vichian 5710546551
 *
 */
public class ClientUI extends JFrame implements Observer {
	private JTextField typeField;
	private Client client;
	private JTextArea onlineArea;
	private JTextArea chatPane;
	private ClassLoader loader;
	private JLabel[] stickerArr;

	/**
	 * Constructor for Client
	 * 
	 * @param client
	 *            is that want to join chat
	 */
	public ClientUI(Client client) {
		super("ClientUI");
		this.client = client;
		initComponents();
		this.setVisible(true);
		setSize(575, 374);
		loader = this.getClass().getClassLoader().getSystemClassLoader();

	}

	/**
	 * Components of ClientUI
	 */
	public void initComponents() {
		Container container = new Container();
		setContentPane(container);
		container.setLayout(null);
		Font font = new Font("Silom", Font.BOLD, 14);

		JPanel headPanel = new JPanel();
		headPanel.setBounds(0, 0, 365, 56);
		container.add(headPanel);
		headPanel.setLayout(null);

		JLabel courseLabel = new JLabel("OOP < 12345678 >");
		courseLabel.setBounds(20, 11, 180, 14);
		headPanel.add(courseLabel);

		JLabel detailLabel = new JLabel("   Course");
		detailLabel.setBounds(20, 31, 180, 14);
		headPanel.add(detailLabel);

		JButton close = new JButton("Sign Out");
		close.setBounds(264, 17, 89, 23);
		headPanel.add(close);
		close.addActionListener(new closeListener());

		JButton toButton = new JButton("To :");
		toButton.setBounds(0, 313, 56, 23);
		container.add(toButton);

		typeField = new JTextField();
		typeField.setBounds(58, 313, 241, 23);
		container.add(typeField);
		typeField.setColumns(10);

		JButton sendButton = new JButton("Send");
		sendButton.setBounds(302, 313, 63, 23);
		sendButton.addActionListener(new sendListener());
		container.add(sendButton);

		chatPane = new JTextArea();
		chatPane.setBounds(0, 0, 4, 22);
		chatPane.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(chatPane);
		scrollPane.setBounds(0, 54, 365, 259);
		scrollPane
		.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		chatPane.setFont(font);
		container.add(scrollPane);
		chatPane.setBackground(null);

		JPanel panel = new JPanel();
		panel.setBounds(368, 0, 191, 126);
		container.add(panel);
		panel.setLayout(null);

		JLabel onlineLabel = new JLabel("Online Users");
		onlineLabel.setBounds(10, 9, 109, 14);
		panel.add(onlineLabel);

		onlineArea = new JTextArea();
		onlineArea.setBounds(10, 29, 157, 130);
		onlineArea.setEditable(false);
		JScrollPane scrollPane2 = new JScrollPane(onlineArea);
		scrollPane2.setBounds(5, 27, 180, 87);
		scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		chatPane.setFont(font);
		panel.add(scrollPane2);

		JLabel sticker = new JLabel("Stickers\r\n");
		sticker.setBounds(378, 137, 109, 14);
		container.add(sticker);

		JPanel stickerPanel = new JPanel();
		stickerPanel.setLayout( new FlowLayout());
		stickerPanel.setPreferredSize(new Dimension(170, 680));

		stickerArr = new JLabel[40];
		
		
		StyleContext context = new StyleContext();
	    StyledDocument document = new DefaultStyledDocument(context);
	    Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);

	    try {
	      document.insertString(document.getLength(), "Ignored", labelStyle);
	    } catch (BadLocationException badLocationException) {
	      System.err.println("Oops");
	    }
	    
		
		
		
		for (int i = 0; i < stickerArr.length; i++) {
			String k = i+1+"";
			stickerArr[i] = new JLabel(new ImageIcon(loader.getSystemResource("images/"+k+".png")));
			stickerArr[i].addMouseListener(new MouseStickerListener());
			StyleConstants.setComponent(labelStyle, stickerArr[i]);
			stickerPanel.add(stickerArr[i]);
		}

		JScrollPane scrollPane3 = new JScrollPane(stickerPanel);
		scrollPane3.setBounds(370, 154, 183, 172);
		container.add(scrollPane3);

		typeField.addActionListener(new sendListener());
		toButton.addActionListener(new ToButtonListener());

	}

	/** ActionListener for To Button **/
	class ToButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			typeField.setText("To: ");
		}

	}

	/** ActionListener for Sending message **/
	class sendListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			try {

				if (typeField.getText().equalsIgnoreCase("quit"))
					client.closeConnection();
				else {
					client.sendToServer(typeField.getText());
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			typeField.setText("");
		}
	}

	/** ActionListener for Sign out from Server **/
	class closeListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				client.sendToServer("logout");
				//				client.closeConnection();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
		}
	}
	/** MouseListener for all stickers **/
	class MouseStickerListener implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			for(int i=0;i<40;i++){
				if(e.getSource()==stickerArr[i])
					try {
						client.sendToServer("%DK-"+i);

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	/** Update UI by notify the change from Observable **/
	@Override
	public void update(Observable ob, Object obj) {

		String msg = (String) obj;

		if (msg.substring(0, 3).equals("@@@")) {
			msg = msg.substring(4);
			String[] parts = msg.split(" ");
			msg = "";
			for (int i = 0; i < parts.length; i++) {
				msg += parts[i] + "\n";
			}
			onlineArea.setText(msg);
		} 
		else if(msg.substring(0, 3).equals("---")){
			String[] i = msg.split(" ");
			int index = Integer.parseInt(i[3].substring(4));
			chatPane.setText(chatPane.getText() + "\n " + i[1]+" "+i[2]+" ");
			chatPane.add(stickerArr[index]);
		}
		else {
				chatPane.setText(chatPane.getText() + "\n " + msg);
				
				chatPane.setCaretPosition(chatPane.getDocument().getLength());
		}

	}
	/** Main method for running UI **/
	public static void main(String[] args) throws IOException {
		// Client client = new Client("dekkaset.ddns.net", 5555,"12345678",
		// "Suzan");
		Client client = new Client("localhost", 5555, "12345678", "Suzan");
		ClientUI ui = new ClientUI(client);
		client.getDispatcher().addObserver(ui);
		client.openConnection();
		client.sendToServer("Login 12345678 kkkk2");

	}

}