package UI;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import Data.KasetLibrary;
import Grade.Grade;
import Grade.GradeCalculator;
import MainApp.DekKASET;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.ImageIcon;
/**
 * User interface for calculate grade application.
 * @author Parisa Supitayakul
 *
 */
public class CalculateGradeUI extends JFrame implements Runnable{
	private DekKASET dekKaset;
	private KasetLibrary library;
	private JPanel contentPane;
	private JLabel lblTotalgrade,lblTotal;
	private List<Grade> gradeList;
	private GradeCalculator gradeCal;
	private JTable calTable;
	private DefaultTableModel dtm;

	/**
	 * Launch the application.
	 */
	 public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculateGradeUI frame = new CalculateGradeUI(new GradeCalculator());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	 }

	 /**
	  * Create the frame.
	  */
	 public CalculateGradeUI(GradeCalculator gradeCal) {
	 	setResizable(false);
	 	setTitle("Grade Calculator");
		 dekKaset = DekKASET.getInstance();
		 library = KasetLibrary.getInstance();
		 this.gradeCal = gradeCal;

		 setBounds(100, 100, 500, 400);
		 contentPane = new JPanel();
		 contentPane.setBackground(new Color(153, 255, 102));
		 contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		 setContentPane(contentPane);

		 Grade[] grade = Grade.values();
		 Object[][] data = new Object[dekKaset.getEnrollList().size()][3];
		 String[] column = new String[]{"CourseID","Course Name","Grade"};
		 for(int i=0;i<dekKaset.getEnrollList().size();i++){
			 data[i][0] = dekKaset.getEnrollList().get(i).getCourseID();
		 }

		 for(int i=0;i<dekKaset.getEnrollList().size();i++){
			 data[i][1] = dekKaset.getEnrollList().get(i).getCourseName();
		 }
		 //data[dekKaset.getEnrollList().size()][1] = "Total";
		 //data[dekKaset.getEnrollList().size()][2] = lblTotalgrade;
		 dtm = new DefaultTableModel(data,column){

			 @Override
			 public boolean isCellEditable(int row, int column) {
				 if(column ==2)
					 return true;

				 return false;
			 }
		 };
		 calTable = new JTable(dtm);

		 TableColumn gradeColumn = calTable.getColumnModel().getColumn(2);
		 JComboBox comboGrade = new JComboBox(grade);
		 List<JComboBox> gradeCombolist = new ArrayList();

		 for(int j=0;j<dekKaset.getEnrollList().size();j++){
			 data[j][2] = comboGrade;
			 gradeCombolist.add(comboGrade);
		 }

		 gradeColumn.setCellEditor(new DefaultCellEditor(comboGrade));

		 lblTotalgrade = new JLabel("");
		 lblTotal = new JLabel("Total :");


		 JButton calculate = new JButton("Calculate");
		 calculate.addActionListener(new ActionListener(){

			 @Override
			 public void actionPerformed(ActionEvent arg0) {
				 gradeList = new ArrayList();
				 for(int i=0;i<gradeCombolist.size();i++){
					 gradeList.add((Grade) gradeCombolist.get(i).getSelectedItem());
					 if(gradeList.get(i) == null){
						 JOptionPane.showConfirmDialog(null,"Please enter grade","Warning",JOptionPane.WARNING_MESSAGE);
						 return;
					 }
				 }
				 double numgrade = gradeCal.calculateGrade(dekKaset.getEnrollList(), gradeList);
				 lblTotalgrade.setText(numgrade+"");

			 }

		 });

		 JButton clear = new JButton("Clear");
		 clear.addActionListener(new ActionListener(){

			 @Override
			 public void actionPerformed(ActionEvent arg0) {
				 lblTotalgrade.setText("");
			 }

		 });
		 contentPane.setLayout(null);

		 JScrollPane scroll = new JScrollPane(calTable);
		 scroll.setBounds(10, 55, 474, 244);
		 //scroll.setBounds(10, 57, 300, 300);
		 contentPane.add(scroll);

		 JPanel result = new JPanel();
		 result.setBounds(10, 299, 474, 24);
		 result.setLayout(new FlowLayout());
		 JPanel buttonRow = new JPanel();
		 buttonRow.setBounds(10, 327, 474, 33);
		 buttonRow.setLayout(new FlowLayout());
		 result.add(lblTotal);
		 result.add(lblTotalgrade);
		 buttonRow.add(calculate);
		 buttonRow.add(clear);
		 contentPane.add(result);
		 contentPane.add(buttonRow);
		 
		 JPanel panel = new JPanel();
		 panel.setBackground(new Color(0, 204, 0));
		 panel.setBounds(0, 0, 494, 50);
		 contentPane.add(panel);
		 panel.setLayout(null);
		 
		 JLabel lblNewLabel_1 = new JLabel("");
		 lblNewLabel_1.setIcon(new ImageIcon(CalculateGradeUI.class.getResource("/images/newGradecal.png")));
		 lblNewLabel_1.setBounds(0, 0, 494, 50);
		 panel.add(lblNewLabel_1);
		 /*JPanel courseNamePane = new JPanel();
    		courseNamePane.setLayout(new BoxLayout(courseNamePane,BoxLayout.Y_AXIS));	

    		lblName = new JLabel("Name");
    		courseNamePane.add(lblName);
    		for(Course course: dekKaset.getEnrollList()){
    			JLabel lblCourse = new JLabel(course.getCourseName());
    			courseNamePane.add(lblCourse);
    			lblCourse.setPreferredSize(new Dimension(200,50));
    		}
    		lblTotal = new JLabel("Total");
    		courseNamePane.add(lblTotal);


    		JPanel gradePane = new JPanel();
    		gradePane.setLayout(new BoxLayout(gradePane,BoxLayout.Y_AXIS));	

    		lblGrade = new JLabel("Grade");
    		gradePane.add(lblGrade);
    		List<JComboBox> gradeCombolist = new ArrayList();
    		for(Course course: dekKaset.getEnrollList()){
    			JComboBox comboGrade = new JComboBox(grade);
    			gradeCombolist.add(comboGrade);
    			gradePane.add(comboGrade);
    			comboGrade.setPreferredSize(new Dimension(50,50));

    		}
    		lblTotalgrade = new JLabel();
    		gradePane.add(lblTotalgrade);

    		gradeList = new ArrayList();

    		JButton calculate = new JButton("Calculate");
    		calculate.addActionListener(new ActionListener(){

    			@Override
    			public void actionPerformed(ActionEvent arg0) {
    				for(int i=0;i<gradeCombolist.size();i++){
    					gradeList = (List<Grade>) gradeCombolist.get(i).getSelectedItem();
    				}
    				double numgrade = gradeCal.calculateGrade(dekKaset.getEnrollList(), gradeList);
    				lblTotalgrade.setText(numgrade+"");
    			}

    		});

    		contentPane.add(courseNamePane,BorderLayout.WEST);
    		contentPane.add(gradePane,BorderLayout.EAST);
		  */
	 }



	 @Override
	 public void run() {
		 this.setVisible(true);
	 }
}

