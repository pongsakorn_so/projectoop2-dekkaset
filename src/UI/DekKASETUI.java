package UI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.Timer;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Clock.CheapClock;
import Clock.Clock;
import Clock.ClockTask;
import DigitalClock.DigitalClockPanel;
import DigitalClock.SuperCheapClock;
import Homework.HomeworkManager;
import MainApp.DekKASET;
import Panel.AppPanelUI;
import Panel.HomeworkPanel;
import Panel.SchedulePanel;

/**
 * User interface to show main menu of program that include many panel such as schedul,app.
 * @author Pongsakorn somsri
 *
 */
public class DekKASETUI extends JFrame implements Runnable{

	private DekKASET dekKaset;


	//panel
	private CheapClock dclock;
	private HomeworkPanel todoListPanel;
	private SchedulePanel schedulePanel;
	private AppPanelUI appPanel;

	private JPanel contentPane;
	private Dimension resolution;
	private JLabel exitB,userPic; 
	private ClassLoader classLoader = this.getClass().getClassLoader();
	private JLabel lblLogout;
	final static long INTERVAL = 500;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DekKASETUI frame = new DekKASETUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	}

	/**
	 * Create the frame.
	 */
	public DekKASETUI() {
		resolution =Toolkit.getDefaultToolkit().getScreenSize();
		dekKaset = DekKASET.getInstance();
		this.initComponent();
	}

	/**
	 * create component.
	 */
	public void initComponent(){

		this.setUndecorated(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this.setBounds(resolution.width-400,0, 400, resolution.height);
		this.setBounds(resolution.width-400,0, 400,768);


		contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane,BoxLayout.Y_AXIS));


		JPanel topPanel = new JPanel();
		topPanel.setBounds(0, 5, 400, 40);
		JPanel userZone = new JPanel();
		userZone.setBackground(new Color(51, 204, 51));
		JPanel menuZone = new JPanel();
		menuZone.setBackground(new Color(51, 204, 51));
		JPanel mainPanel = new JPanel();	


		FlowLayout userZoneLayout = new FlowLayout();
		FlowLayout menuZoneLayout = new FlowLayout();

		userZoneLayout.setAlignment(FlowLayout.LEFT);
		menuZoneLayout.setAlignment(FlowLayout.RIGHT);
		topPanel.setPreferredSize(new Dimension(400,40));

		topPanel.setLayout(new GridLayout(1,2));
		userZone.setLayout(new FlowLayout());
		userZone.setLayout(userZoneLayout);
		menuZone.setLayout(menuZoneLayout);




		this.setContentPane(contentPane);
		this.setBackground(Color.black);
		Clock clock =Clock.getInstance();
		ClockTask ct = new ClockTask(clock);
		Timer timer = new Timer();
		long delay = 1000 - System.currentTimeMillis()%1000;
		timer.scheduleAtFixedRate( ct, delay, INTERVAL );
		dclock = new CheapClock(clock);
		clock.addObserver(dclock);
		dclock.setBounds(0, 50, 400, 150);
		
		schedulePanel = new SchedulePanel();
		schedulePanel.setBounds(0, 210, 400, 240);
		todoListPanel = new HomeworkPanel(new HomeworkManager());
		todoListPanel.setBounds(0,460, 400, 200);
		appPanel = new AppPanelUI();
		appPanel.setBounds(0, 610, 400, 120);
		//clock.setBackground(Color.black);
		mainPanel.setSize(300, resolution.height);
		mainPanel.setLayout(null);
		mainPanel.add(topPanel);
		mainPanel.add(dclock);
		mainPanel.add(schedulePanel);
		mainPanel.add(todoListPanel);
		mainPanel.add(appPanel);

		dekKaset.addObserver(schedulePanel);
		dekKaset.addObserver(todoListPanel);
		//clock.addObserver(homeworkP);

		contentPane.add(mainPanel);

		userPic = new JLabel();
		exitB = new JLabel();
		userPic.setIcon(new ImageIcon(classLoader.getResource("images/profile.png")));
		userPic.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				ProfileUI ui = new ProfileUI(dekKaset.getUser());
				ui.run();
			}
		});
		exitB.setIcon(new ImageIcon(DekKASETUI.class.getResource("/images/closeBt.png")));
		exitB.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				DekKASETUI.this.setVisible(false);
				dekKaset.setUser(null);
				LoginUI ui = new LoginUI();
				ui.run();
			}
		});
		
		userZone.add(userPic);
		userZone.add(new JLabel(dekKaset.getUser().getUsername()));
		
		lblLogout = new JLabel("LOGOUT");
		menuZone.add(lblLogout);
		menuZone.add(exitB);
		topPanel.add(userZone);
		topPanel.add(menuZone);

		mainPanel.setBackground(Color.GREEN);
		
		lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(DekKASETUI.class.getResource("/images/Green-Background-08.jpg")));
		lblNewLabel.setBounds(0, 0, 400, 768);
		mainPanel.add(lblNewLabel);
		//mainPanel.setForeground(Color.f);



	}

	@Override
	public void run() {
		this.setVisible(true);
	}

}
