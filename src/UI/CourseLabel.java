package UI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import Course.Course;

/**
 * Label of course that including action for open course detail.
 * @author Gateway
 *
 */
public class CourseLabel extends JLabel{
	private Course course;
	/**
	 * Constructor for setting label.
	 * @param course that used for create Label.
	 */
	public CourseLabel(Course course){
		this.course= course;
		this.setOpaque(true);
		this.setBackground(Color.GRAY);
		this.setPreferredSize(new Dimension(300,30));
		this.initComponent();
		this.setText(course.getCourseName());
		this.setHorizontalAlignment(SwingConstants.CENTER);

	}
	/**
	 * To create component.
	 */
	public void initComponent(){
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				CourseDetailUI cd = new CourseDetailUI(course);
				cd.run();
			}
		});
	}
}
