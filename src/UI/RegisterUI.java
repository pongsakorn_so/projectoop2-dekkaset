package UI;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import Data.Database;

/**
 * User interface form that user will fill to register.
 * @author Parisa Supitayakul 
 *
 */
public class RegisterUI extends JFrame implements Runnable{

	private JPanel contentPane;
	private JTextField TFUsername;
	private JPasswordField TFPassword;
	private JPasswordField TFConPass;
	private JTextField TFFirstname;
	private JTextField TFLastname;
	private JTextField TFEmail;
	private Database database;

	/**
	 * Launch the application.c
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterUI frame = new RegisterUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterUI() {
		this.database = Database.getInstance();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblRegister = new JLabel("Register");
		lblRegister.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblRegister.setBounds(111, 24, 67, 21);
		contentPane.add(lblRegister);

		JLabel lblUser = new JLabel("Username");
		lblUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblUser.setBounds(10, 80, 74, 14);
		contentPane.add(lblUser);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPassword.setBounds(10, 120, 74, 14);
		contentPane.add(lblPassword);

		JLabel lblConfirmPassword = new JLabel("Confirm Password");
		lblConfirmPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblConfirmPassword.setBounds(10, 160, 123, 14);
		contentPane.add(lblConfirmPassword);

		JLabel lblFirstname = new JLabel("Firstname");
		lblFirstname.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblFirstname.setBounds(10, 200, 74, 14);
		contentPane.add(lblFirstname);

		JLabel lblLastname = new JLabel("Lastname");
		lblLastname.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblLastname.setBounds(10, 240, 74, 14);
		contentPane.add(lblLastname);

		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblEmail.setBounds(10, 280, 46, 14);
		contentPane.add(lblEmail);

		TFUsername = new JTextField();
		TFUsername.setBounds(91, 79, 215, 20);
		contentPane.add(TFUsername);
		TFUsername.setColumns(10);

		TFPassword = new JPasswordField();
		TFPassword.setColumns(10);
		TFPassword.setBounds(91, 119, 256, 20);
		TFPassword.setEditable(false);
		contentPane.add(TFPassword);

		TFConPass = new JPasswordField();
		TFConPass.setColumns(10);
		TFConPass.setBounds(129, 159, 218, 20);
		TFConPass.setEditable(false);
		contentPane.add(TFConPass);

		TFFirstname = new JTextField();
		TFFirstname.setColumns(10);
		TFFirstname.setBounds(91, 199, 256, 20);
		TFFirstname.setEditable(false);
		contentPane.add(TFFirstname);

		TFLastname = new JTextField();
		TFLastname.setColumns(10);
		TFLastname.setBounds(91, 239, 256, 20);
		TFLastname.setEditable(false);
		contentPane.add(TFLastname);

		TFEmail = new JTextField();
		TFEmail.setColumns(10);
		TFEmail.setBounds(91, 279, 256, 20);
		TFEmail.setEditable(false);
		contentPane.add(TFEmail);

		JLabel lblCheckUser = new JLabel("");
		//lblCheckUser.setIcon(new ImageIcon(RegisterUI.class.getResource("images/SettingB.png")));
		lblCheckUser.setBounds(357, 80, 17, 14);
		contentPane.add(lblCheckUser);

		JButton btnCheckUser = new JButton("Check");
		btnCheckUser.setBounds(316, 78, 31, 23);
		contentPane.add(btnCheckUser);

		JButton btnRegister = new JButton("Register");
		btnRegister.setBounds(180, 338, 89, 23);
		contentPane.add(btnRegister);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(274, 338, 89, 23);
		contentPane.add(btnCancel);

		JButton btnClear = new JButton("Clear");
		btnClear.setBounds(10, 338, 89, 23);
		contentPane.add(btnClear);

		btnCheckUser.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!database.isDuplicateUsername(TFUsername.getText())){
					TFUsername.setEditable(false);
					TFPassword.setEditable(true);
					TFConPass.setEditable(true);
					TFFirstname.setEditable(true);
					TFLastname.setEditable(true);
					TFEmail.setEditable(true);
				}else{
					JOptionPane.showMessageDialog(null, "This username is already use", "WARNING",JOptionPane.WARNING_MESSAGE);
				}
			}

		});

		btnRegister.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String username = TFUsername.getText();
				String password = String.valueOf(TFPassword.getPassword());
				String passwordCon = String.valueOf(TFConPass.getPassword());
				String firstName = TFFirstname.getText();
				String lastName = TFLastname.getText();
				String email = TFEmail.getText();
				
				if(!password.equals(passwordCon)){
					JOptionPane.showMessageDialog(null, "Passwords don't match ", "WARNING",JOptionPane.WARNING_MESSAGE);
				}
				else if(!isEmail(email)){
					JOptionPane.showMessageDialog(null, "This is not email", "WARNING",JOptionPane.WARNING_MESSAGE);
				}
				else{
					database.addUserToDB(username, password, firstName, lastName, email);
					RegisterUI.this.setVisible(false);
				}
			}

		});

		btnCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				RegisterUI.this.setVisible(false);
			}

		});

		btnClear.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				TFUsername.setEditable(true);
				TFUsername.setText("");
				TFPassword.setEditable(false);
				TFPassword.setText("");
				TFConPass.setEditable(false);
				TFConPass.setText("");
				TFFirstname.setEditable(false);
				TFFirstname.setText("");
				TFLastname.setEditable(false);
				TFEmail.setText("");
				TFEmail.setEditable(false);
			}

		});
	}
	/**
	 * To check it is an email or not
	 * @param emails that used for check.
	 * @return true if it's email , false if it isn't.
	 */
	public boolean isEmail(String emails){
		char[] emailCharlist = emails.toCharArray();
		for(char email : emailCharlist){
			if(email == '@'){
				return true;
			}
		}
		return false;
	}

	@Override
	public void run() {
		this.setVisible(true);
	}
}