package UI;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ChatServer.Client;
import Course.Course;
import Homework.Homework;
import MainApp.DekKASET;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * User interface to show detail of each course.
 * @author Pongsakorn Somsri
 *
 */
public class CourseDetailUI extends JFrame implements Runnable{

	private JLabel courseIDLB,courseNameLB,courseCreditLB,courseDesLB,courseLB,homeworkLB;
	private JTextField courseIDTF,courseNameTF,courseCreditTF;
	private JTextArea courseDesTA;
	private Course course;

	public CourseDetailUI(Course course) {
		this.course = course;
		this.initComponent();
		this.pack();
	}

	/**
	 * To create component.
	 */
	public void initComponent(){
		Container contentPane = this.getContentPane();
		JPanel detailPanel = new JPanel();
		JPanel courseIDPanel= new JPanel();
		JPanel courseNamePanel = new JPanel();
		JPanel courseCreditPanel = new JPanel();
		JPanel courseDesLBPanel = new JPanel();
		JPanel courseDesTAPanel = new JPanel();

		JScrollPane homeworkPanel = new JScrollPane(this.getHomeworkListTable());


		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		detailPanel.setLayout(new BoxLayout(detailPanel, BoxLayout.Y_AXIS));

		FlowLayout detailLayout = new FlowLayout();
		detailLayout.setAlignment(FlowLayout.LEFT);

		courseIDPanel.setLayout( detailLayout);
		courseNamePanel.setLayout( detailLayout);
		courseCreditPanel.setLayout( detailLayout);
		courseDesLBPanel.setLayout( detailLayout);
		courseDesTAPanel.setLayout( detailLayout);

		courseLB = new JLabel("Course Detail");
		homeworkLB = new JLabel("Homework list");

		courseIDLB = new JLabel("ID:       ");
		courseIDTF = new JTextField(10);
		courseIDTF.setEditable(false);

		courseNameLB = new JLabel("Name:");
		courseNameTF = new JTextField(10);
		courseNameTF.setEditable(false);

		courseCreditLB = new JLabel("Credit:");
		courseCreditTF = new JTextField(5);
		courseCreditTF.setEditable(false);

		courseDesLB = new JLabel("Description:");
		courseDesTA = new JTextArea();
		courseDesTA.setEditable(false);

		courseDesTA.setPreferredSize(new Dimension(500,100));

		courseIDPanel.add(courseIDLB);
		courseIDPanel.add(courseIDTF);

		courseNamePanel.add(courseNameLB);
		courseNamePanel.add(courseNameTF);

		courseCreditPanel.add(courseCreditLB);
		courseCreditPanel.add(courseCreditTF);

		courseDesLBPanel.add(courseDesLB);
		courseDesTAPanel.add(new JScrollPane(courseDesTA));

		detailPanel.add(courseIDPanel);
		detailPanel.add(courseNamePanel);
		detailPanel.add(courseCreditPanel);
		detailPanel.add(courseDesLBPanel);
		detailPanel.add(courseDesTAPanel);

		courseIDTF.setText(course.getCourseID());
		courseNameTF.setText(course.getCourseName());
		courseCreditTF.setText(course.getCourseCredit()+"");
		courseDesTA.setText(course.getCourseDescription());

		homeworkPanel.setPreferredSize(new Dimension(500,300));

		JPanel courseLBP = new JPanel();
		courseLBP.setLayout(detailLayout);
		courseLBP.add(courseLB);

		JPanel homeworkLBP = new JPanel();
		homeworkLBP.setLayout(detailLayout);
		homeworkLBP.add(homeworkLB);


		contentPane.add(courseLBP);
		contentPane.add(detailPanel);
		contentPane.add(homeworkLBP);
		contentPane.add(homeworkPanel);

		JButton groupB = new JButton("Group chat");
		groupB.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					Client client = new Client("localhost", 5555,course.getCourseID(), course.getCourseName());
					ClientUI ui = new ClientUI(client);
					client.getDispatcher().addObserver(ui);
					client.openConnection();
					client.sendToServer("Login "+course.getCourseID()+" "+DekKASET.getInstance().getUser().getUsername());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		});
		contentPane.add(groupB);



	}

	/**
	 * To get homework list in table.
	 * @return table that including homework list.
	 */
	public JTable getHomeworkListTable(){
		List<Homework> homeworkList =  course.getHomeworkList();
		Object[][] data = new Object[homeworkList.size()][3] ;
		String[] column = new String[]{"HomeworkName","Description","DeadLine"};
		for(int i=0;i<homeworkList.size();i++){
			data[i][0] = homeworkList.get(i).getHomeworkName();
			data[i][1] = homeworkList.get(i).getDescription();
			data[i][2] = homeworkList.get(i).getDeadLine().toString();
		}
		DefaultTableModel homeworkTable = new DefaultTableModel(data,column){

			@Override
			public boolean isCellEditable(int row, int column) {
				//all cells false
				return false;
			}
		};

		JTable table = new JTable(homeworkTable);
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println(((JTable)e.getSource()).getSelectedRow());
				
				if (e.getClickCount() == 2) {
					JTable target = ((JTable)e.getSource());
					int row = target.getSelectedRow();
					Homework selectedHomework = course.getHomeworkList().get(row);
					HomeworkDetailUI ui = new HomeworkDetailUI(selectedHomework);
					ui.run();
					// do some action if appropriate column
				}
			}
		});
		
		table.getColumnModel().getColumn(0).setPreferredWidth(1);
		table.getColumnModel().getColumn(1).setPreferredWidth(1);

		table.getTableHeader().setEnabled(false);
		table.setFillsViewportHeight(true);
		table.setPreferredScrollableViewportSize(new Dimension(500,300));

		return table;
	}

	@Override
	public void run() {
		this.setVisible(true);
	}

}