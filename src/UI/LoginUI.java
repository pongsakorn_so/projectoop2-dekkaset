package UI;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import MainApp.DekKASET;
import User.User;
import Data.Database;
import Data.KasetLibrary;

import java.awt.Font;
import javax.swing.ImageIcon;

/**
 * User interface to show login page.
 * @author Pongsakorn Somsri
 *
 */
public class LoginUI extends JFrame implements Runnable{

	private JPanel contentPane;
	private JTextField userNameTF;
	private JPasswordField passwordField;
	private Database database;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginUI frame = new LoginUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginUI() {
		setTitle("LOGIN");
		setResizable(false);

		this.database = Database.getInstance();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 500);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(127, 255, 0));
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsername = new JLabel("USERNAME");
		lblUsername.setForeground(Color.WHITE);
		lblUsername.setBackground(Color.RED);
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsername.setBounds(20, 230, 70, 30);
		contentPane.add(lblUsername);

		userNameTF = new JTextField();
		userNameTF.setBounds(100, 230, 170, 30);
		contentPane.add(userNameTF);
		userNameTF.setColumns(10);

		JLabel lblPassword = new JLabel("PASSWORD");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setBounds(20, 280, 70, 30);
		contentPane.add(lblPassword);

		passwordField = new JPasswordField();
		passwordField.setBounds(100, 280, 170, 30);
		contentPane.add(passwordField);

		JButton btnLogin = new JButton("");
		btnLogin.setIcon(new ImageIcon(LoginUI.class.getResource("/images/login.png")));
		btnLogin.setBounds(30, 340, 90, 25);
		contentPane.add(btnLogin);
		btnLogin.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String username = userNameTF.getText();
				String password = String.valueOf(passwordField.getPassword());
		
				User user = database.getUserLogin(username,password);
				if(user==null){
					JOptionPane.showMessageDialog(null, "User not found!", "WARNING",JOptionPane.WARNING_MESSAGE);
				}
				else{
					KasetLibrary library = KasetLibrary.getInstance();
					DekKASET dekKaset = DekKASET.getInstance();
					library.updateLibrary();
					database.addEnrollListToUser(user);
					dekKaset.setUser(user);
					
					
					DekKASETUI mainUI = new DekKASETUI();
					mainUI.run();
					dekKaset.printAllCourse();
					LoginUI.this.setVisible(false);
				}
			}

		});

		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(LoginUI.class.getResource("/images/register.png")));
		btnNewButton.setBounds(165, 340, 90, 25);
		contentPane.add(btnNewButton);
		
		JLabel lblDekkaset = new JLabel("");
		lblDekkaset.setIcon(new ImageIcon(LoginUI.class.getResource("/images/dekkaset.png")));
		lblDekkaset.setForeground(Color.WHITE);
		lblDekkaset.setFont(new Font("Tahoma", Font.BOLD, 44));
		lblDekkaset.setBounds(-14, 72, 284, 77);
		contentPane.add(lblDekkaset);
		btnNewButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				RegisterUI ui = new RegisterUI();
				ui.run();
			}
		});
	}

	@Override
	public void run() {
		this.setVisible(true);
	}
}
