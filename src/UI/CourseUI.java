package UI;
import javax.swing.JFrame;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import Course.Course;
import MainApp.DekKASET;

import javax.swing.JPanel;
import javax.swing.ImageIcon;

import java.awt.Color;

/**
 * User interface to show list of all course.
 * @author Chinatip vichian
 *
 */
public class CourseUI extends JFrame implements Runnable{
	private DekKASET dekKU;
	private Object[][] data;
	public CourseUI(){
		super("CourseUI");
		setTitle("All Course");
		this.dekKU = DekKASET.getInstance();
		this.data = new Object[dekKU.getAllCourse().size()][4];
		this.putData();
		this.initComponents();
		this.setVisible(true);
	}
	/**
	 * To add data into list of course.
	 */
	private void putData(){
		List<Course> allCourse = dekKU.getAllCourse();

		for(int i=0;i<allCourse.size();i++){
			Course course = allCourse.get(i);
			data[i][0] = course.getCourseID();
			data[i][1] = course.getCourseName();
			data[i][2] = course.getCourseDescription();
			data[i][3] = course.getCourseCredit();
		}
	}
	/**
	 * To create component.
	 */
	public void initComponents(){
		this.setPreferredSize(new Dimension(500,400));
		Container container = new Container();
		this.setContentPane( container);
		String[] columnNames = {"CourseID","Name","Description","Credit"};
		
		DefaultTableModel courseTable = new DefaultTableModel(data,columnNames){
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		container.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 494, 50);
		container.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(CourseUI.class.getResource("/images/newCourseLibr.png")));
		lblNewLabel.setBounds(0, 0, 494, 50);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(153, 255, 102));
		panel_1.setBounds(0, 51, 494, 370);
		container.add(panel_1);
		panel_1.setLayout(null);
		
		JTable table = new JTable(courseTable);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(21, 5, 452, 291);
		panel_1.add(scrollPane);
		this.pack();
		//table.setEnabled(false);

		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println(((JTable)e.getSource()).getSelectedRow());
				
				if (e.getClickCount() == 2) {
					JTable target = ((JTable)e.getSource());
					int row = target.getSelectedRow();
					Course selectedCourse = dekKU.getAllCourse().get(row);
					CourseDetailUI ui = new CourseDetailUI(selectedCourse);
					ui.run();
					// do some action if appropriate column
				}
			}
		});
		
		
				//table.setFillsViewportHeight(true);
	}
	@Override
	public void run() {
		this.setVisible(true);
	}
}