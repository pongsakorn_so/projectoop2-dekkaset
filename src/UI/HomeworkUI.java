package UI;
import javax.swing.JFrame;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import Homework.Homework;
import Homework.HomeworkManager;
import MainApp.DekKASET;

import javax.swing.JPanel;
import javax.swing.ImageIcon;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * User interface to show list of all homework.
 * @author Chinatip vichian
 *
 */
public class HomeworkUI extends JFrame implements Runnable{
	private DekKASET dekKU;
	private HomeworkManager hm;
	private DefaultTableModel homeworkTable;
	private Object[][] data;
	public HomeworkUI(HomeworkManager hm){
		
		super("HomeworkUI");
		setTitle("Homework List");
		this.dekKU = DekKASET.getInstance();
		this.data = new Object[dekKU.getUserHomework().size()][4];
		this.hm = hm;
		this.putData();
		this.initComponents();
		this.setVisible(true);
	}
	/**
	 * To add data into list of homework.
	 */
	private void putData(){
		data = new Object[dekKU.getUserHomework().size()][4];
		List<Homework> allHomework = dekKU.getUserHomework();
		hm.getSortedAllHomework(allHomework);
		for(int i=0;i<allHomework.size();i++){
			Homework homework = allHomework.get(i);
			data[i][0] = homework.getCourseID();
			data[i][1] = homework.getHomeworkName();
			data[i][2] = homework.getDescription();
			data[i][3] = homework.deadLineToString();
		}
	}
	/**
	 * To create component.
	 */
	public void initComponents(){
		this.setPreferredSize(new Dimension(500, 400));
		Container container = new Container();
		this.setContentPane( container);
		String[] columnNames = {"CourseID","Name","Description","Deadline"};
		
		homeworkTable = new DefaultTableModel(data,columnNames){
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		
		JTable table = new JTable(homeworkTable);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 61, 464, 289);
		//table.setEnabled(false);

		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println(((JTable)e.getSource()).getSelectedRow());
				
				if (e.getClickCount() == 2) {
					JTable target = ((JTable)e.getSource());
					int row = target.getSelectedRow();
					Homework selectedHomework = dekKU.getUserHomework().get(row);
					HomeworkDetailUI ui = new HomeworkDetailUI(selectedHomework);
					ui.run();
					// do some action if appropriate column
				}
			}
		});
		container.setLayout(null);

		table.setFillsViewportHeight(true);
		container.add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 494, 50);
		container.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(HomeworkUI.class.getResource("/images/newHW.png")));
		lblNewLabel.setBounds(0, 0, 494, 50);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(153, 255, 102));
		panel_1.setBounds(0, 42, 494, 379);
		container.add(panel_1);
		panel_1.setLayout(null);
		
		this.pack();
	}
	@Override
	public void run() {
		this.setVisible(true);
	}
}