package UI;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Homework.Homework;

/**
 * User interface to show homework detail.
 * @author Pongsakorn Somsri
 *
 */
public class HomeworkDetailUI extends JFrame implements Runnable{
	private JLabel courseIDLB,courseNameLB,courseCreditLB,courseDesLB,courseLB,homeworkLB;
	private JTextField courseIDTF,courseNameTF,courseCreditTF;
	private JTextArea courseDesTA;
	private Homework homework;
	
	/**
	 * Constructor for declare variable.
	 * @param homework that used to show.
	 */
	public HomeworkDetailUI(Homework homework) {
		this.homework = homework;
		this.initComponent();
		this.pack();
	}
	
	/**
	 * To create component. 
	 */
	public void initComponent(){
		Container contentPane = this.getContentPane();
		JPanel topPanel = new JPanel();
		JPanel detailPanel = new JPanel();
		JPanel courseIDPanel= new JPanel();
		JPanel courseNamePanel = new JPanel();
		JPanel courseCreditPanel = new JPanel();
		JPanel courseDesLBPanel = new JPanel();
		JPanel courseDesTAPanel = new JPanel();
		
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		detailPanel.setLayout(new BoxLayout(detailPanel, BoxLayout.Y_AXIS));
		
		FlowLayout detailLayout = new FlowLayout();
		detailLayout.setAlignment(FlowLayout.LEFT);
		
		topPanel.setLayout(detailLayout);
		courseIDPanel.setLayout( detailLayout);
		courseNamePanel.setLayout( detailLayout);
		courseCreditPanel.setLayout( detailLayout);
		courseDesLBPanel.setLayout( detailLayout);
		courseDesTAPanel.setLayout( detailLayout);
		
		courseLB = new JLabel("Homework Detail");
		
		courseIDLB = new JLabel("ID:       ");
		courseIDTF = new JTextField(10);
		courseIDTF.setEditable(false);
		
		courseNameLB = new JLabel("Name:");
		courseNameTF = new JTextField(10);
		courseNameTF.setEditable(false);
		
		courseCreditLB = new JLabel("DeadLine:");
		courseCreditTF = new JTextField(10);
		courseCreditTF.setEditable(false);
		
		courseDesLB = new JLabel("Description:");
		courseDesTA = new JTextArea();
		courseDesTA.setEditable(false);
		
		courseDesTA.setPreferredSize(new Dimension(500,100));
		
		
		courseNamePanel.add(courseNameLB);
		courseNamePanel.add(courseNameTF);
		
		courseCreditPanel.add(courseCreditLB);
		courseCreditPanel.add(courseCreditTF);
		
		courseDesLBPanel.add(courseDesLB);
		courseDesTAPanel.add(new JScrollPane(courseDesTA));
		
		detailPanel.add(courseNamePanel);
		detailPanel.add(courseCreditPanel);
		detailPanel.add(courseDesLBPanel);
		detailPanel.add(courseDesTAPanel);
		
		courseNameTF.setText(homework.getHomeworkName());
		courseCreditTF.setText(homework.deadLineToString());
		courseDesTA.setText(homework.getDescription());
		
		topPanel.add(courseLB);
		contentPane.add(topPanel);
		contentPane.add(detailPanel);
		
	}
	

	@Override
	public void run() {
		this.setResizable(false);
		this.setVisible(true);
	}
}
