package UI;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.UIManager;

import Course.Course;
import Data.KasetLibrary;
import MainApp.DekKASET;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.ImageIcon;

/**
 * User interface for manage course including add course and remove course. 
 * @author Parisa Supitayakul
 *
 */
public class CourseManagementUI extends JFrame implements Runnable{
	protected DekKASET dekKaset;
	private KasetLibrary library;
	private JPanel contentPane;
	protected JTable viewTable;
	private JTextField courseIDTF;
	private DefaultTableModel dtm;
	/**
	 * Launch the application.
	 */
	 public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CourseManagementUI frame = new CourseManagementUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	 }
	 /**
	  * Create the frame.
	  */
	 public CourseManagementUI() {
	 	setResizable(false);
	 	setTitle("Course Management");
		 dekKaset = DekKASET.getInstance();
		 library = KasetLibrary.getInstance();

		 setBounds(100, 100, 500, 600);
		 contentPane = new JPanel();
		 contentPane.setBackground(new Color(153, 255, 102));
		 contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		 setContentPane(contentPane);
		 contentPane.setLayout(null);

		 JPanel viewPanel = new JPanel();
		 viewPanel.setBackground(new Color(153, 255, 102));
		 viewPanel.setBounds(10, 53, 474, 227);
		 contentPane.add(viewPanel);
		 viewPanel.setLayout(null);

		 JLabel lblViewCourse = new JLabel("View Course");
		 lblViewCourse.setBounds(10, 11, 69, 14);
		 viewPanel.add(lblViewCourse);

		 //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
		 List<Course> enlist = dekKaset.getEnrollList();

		 Object[][] data = new Object[enlist.size()][5];
		 String[] column = new String[]{"CourseID","Course Name","Time","Credit","remove"};

		 for(int i=0;i<dekKaset.getEnrollList().size();i++){
			 data[i][0] = dekKaset.getEnrollList().get(i).getCourseID();
			 data[i][1] = dekKaset.getEnrollList().get(i).getCourseName();
			 data[i][2] = dekKaset.getEnrollList().get(i).getCourseTimeList();
			 data[i][3] = dekKaset.getEnrollList().get(i).getCourseCredit();
			 data[i][4] = "Remove";
		 }
		 dtm = new DefaultTableModel(data,column){

			 @Override
			 public boolean isCellEditable(int row, int column) {
				 //all cells false
				 return false;
			 }
		 };

		 viewTable = new JTable(dtm);

		 viewTable.addMouseListener(new MouseAdapter() {
			 public void mouseClicked(MouseEvent e) {
				 if (e.getClickCount() == 1) {
					 JTable target = (JTable)e.getSource();
					 int row = target.getSelectedRow();
					 int column = target.getSelectedColumn();
					 if(column == 4){
						 int confirm = JOptionPane.showConfirmDialog(null,"Are you sure?","Remove",JOptionPane.YES_NO_OPTION);

						 //int column = viewTable.getEditingColumn();
						 DefaultTableModel model = (DefaultTableModel) viewTable.getModel();

						 String courseID = dekKaset.getUser().getEnrollList().get(row);
						 System.out.println(courseID);
						 if(confirm == JOptionPane.YES_OPTION){
							 model.removeRow(row);
							 System.out.println(dekKaset.removeCourseFromUser(courseID));
						 }
					 }
				 }
			 }
		 });
		 JScrollPane scroll = new JScrollPane(viewTable);
		 scroll.setBounds(10, 26, 454, 201);
		 viewPanel.add(scroll);

		 JPanel addPanel = new JPanel();
		 addPanel.setBackground(new Color(153, 255, 102));
		 addPanel.setBounds(10, 280, 474, 275);
		 contentPane.add(addPanel);
		 addPanel.setLayout(null);

		 JLabel lblAddCourse = new JLabel("Add Course");
		 lblAddCourse.setFont(new Font("Tahoma", Font.PLAIN, 15));
		 lblAddCourse.setBounds(10, 11, 86, 14);
		 addPanel.add(lblAddCourse);

		 JLabel lblCourseId = new JLabel("Course ID :");
		 lblCourseId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		 lblCourseId.setBounds(10, 50, 86, 14);
		 addPanel.add(lblCourseId);

		 JLabel lblCourseName = new JLabel("Course Name :");
		 lblCourseName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		 lblCourseName.setBounds(10, 89, 102, 14);
		 addPanel.add(lblCourseName);

		 JLabel lblTime = new JLabel("Time  :");
		 lblTime.setFont(new Font("Tahoma", Font.PLAIN, 13));
		 lblTime.setBounds(10, 168, 57, 14);
		 addPanel.add(lblTime);

		 JLabel lblCredit = new JLabel("Credit :");
		 lblCredit.setFont(new Font("Tahoma", Font.PLAIN, 13));
		 lblCredit.setBounds(10, 127, 57, 14);
		 addPanel.add(lblCredit);

		 courseIDTF = new JTextField();
		 courseIDTF.setBounds(98, 49, 149, 20);
		 addPanel.add(courseIDTF);
		 courseIDTF.setColumns(10);

		 JLabel courseNameTF = new JLabel("CourseName");
		 courseNameTF.setBounds(122, 91, 156, 14);
		 addPanel.add(courseNameTF);

		 JLabel creditTF = new JLabel("Credit");
		 creditTF.setBounds(70, 128, 156, 14);
		 addPanel.add(creditTF);

		 JLabel timeTF = new JLabel("Time");
		 timeTF.setBounds(70, 169, 194, 14);
		 addPanel.add(timeTF);

		 JButton btncheckCourse = new JButton("Check");
		 btncheckCourse.setBounds(257, 48, 70, 23);
		 addPanel.add(btncheckCourse);

		 JButton btnClear = new JButton("Clear");
		 btnClear.setBounds(284, 241, 70, 23);
		 addPanel.add(btnClear);

		 JButton btnSave = new JButton("Save");
		 btnSave.setBounds(108, 241, 70, 23);
		 addPanel.add(btnSave);

		 JButton btnCancel = new JButton("Cancel");
		 btnCancel.setBounds(197, 241, 70, 23);
		 addPanel.add(btnCancel);

		 btnClear.setEnabled(false);
		 btnSave.setEnabled(false);
		 btnCancel.setEnabled(false);
		 
		 JPanel panel = new JPanel();
		 panel.setBackground(new Color(0, 204, 0));
		 panel.setBounds(0, 0, 494, 50);
		 contentPane.add(panel);
		 panel.setLayout(null);
		 
		 JLabel lblNewLabel = new JLabel("New label");
		 lblNewLabel.setBounds(0, 0, 494, 50);
		 panel.add(lblNewLabel);
		 lblNewLabel.setIcon(new ImageIcon(CourseManagementUI.class.getResource("/images/newCourseMan.png")));

		 btncheckCourse.addActionListener(new ActionListener(){
			 @Override
			 public void actionPerformed(ActionEvent arg0) {
				 String courseID = courseIDTF.getText();
				 for(Course course : dekKaset.getAllCourse()){
					 if(courseID.equals(course.getCourseID())){
						 //hasCourse = true;
						 courseNameTF.setText(course.getCourseName());
						 creditTF.setText(course.getCourseCredit()+"");
						 timeTF.setText(course.getCourseTimeList().toString());

						 btnClear.setEnabled(true);
						 btnSave.setEnabled(true);
						 btnCancel.setEnabled(true);

						 return;
					 }
				 }
				 JOptionPane.showMessageDialog(null,"Course ID not found!","Not Found!",JOptionPane.WARNING_MESSAGE);
			 }

		 });

		 btnClear.addActionListener(new ActionListener(){
			 @Override
			 public void actionPerformed(ActionEvent e) {
				 courseIDTF.setText("");
				 courseNameTF.setText("");
				 creditTF.setText("");
				 timeTF.setText("");

				 btnClear.setEnabled(false);
				 btnSave.setEnabled(false);
				 btnCancel.setEnabled(false);
			 }
		 });

		 btnSave.addActionListener(new ActionListener(){
			 @Override
			 public void actionPerformed(ActionEvent e) {
				 String courseID = courseIDTF.getText();
				 if(library.hasThisCourse(courseID)&&!dekKaset.getUser().hasThisCourse(courseID)){
					 //dtm.removeRow(dtm.getRowCount()-1);
					 dekKaset.addCourseToUser(courseIDTF.getText());
					 dtm.addRow(new Object[] {courseIDTF.getText(),courseNameTF.getText(),timeTF.getText(),creditTF.getText(),"Remove"});

				 }
				 else {
					 JOptionPane.showMessageDialog(null,"have this course","Same Course!",JOptionPane.WARNING_MESSAGE);
				 }
			 }

		 });

		 btnCancel.addActionListener(new ActionListener(){
			 @Override
			 public void actionPerformed(ActionEvent e) {
				 contentPane.setVisible(false);
			 }

		 });
	 }
	 @Override
	 public void run() {
		 this.setVisible(true);
	 }

}

