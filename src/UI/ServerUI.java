package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import ChatServer.Server;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class ServerUI {

	private JFrame frame;
	private Server server;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServerUI window = new ServerUI(new Server(5555));
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ServerUI(Server server) {
		this.server = server;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnOpenServer = new JButton("OPEN SERVER");
		btnOpenServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Starting Server");
				try {
					server.listen();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					System.out.println("Fail to connect to Server");
					e1.printStackTrace();
				}
			}
		});
		btnOpenServer.setBounds(58, 32, 173, 38);
		frame.getContentPane().add(btnOpenServer);
		
		JButton button = new JButton("Close SERVER");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					System.out.println("Close server");
					server.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		button.setBounds(58, 96, 173, 38);
		frame.getContentPane().add(button);
	}
}
