package UI;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Calendar.CalendarEvent;
import Data.KasetLibrary;

import com.toedter.calendar.JCalendar;

import javax.swing.JLabel;

import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JTextArea;
/**
 * User Interface to show calendar.
 * @author Parisa Supitayakul
 *
 */
public class CalendarUI extends JFrame implements Runnable{

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalendarUI frame = new CalendarUI();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CalendarUI() {
		setBounds(100, 100, 299, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JCalendar calendar = new JCalendar();
		calendar.setBounds(10, 36, 264, 190);
		contentPane.add(calendar);

		JLabel lblCalendar = new JLabel("Calendar");
		lblCalendar.setBounds(99, 11, 68, 14);
		lblCalendar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblCalendar);

		JTextArea textAreaEvent = new JTextArea();
		textAreaEvent.setBounds(10, 252, 264, 183);
		contentPane.add(textAreaEvent);

		calendar.getDayChooser().addPropertyChangeListener("day", new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent e) {

				
				int day = calendar.getDayChooser().getDay();
				int month = calendar.getMonthChooser().getMonth();
				int year = calendar.getYearChooser().getYear();
				System.out.println(day+"/"+(month+1)+"/"+year);
				textAreaEvent.setText("");
				for(CalendarEvent ce : KasetLibrary.getInstance().getCalendarList()){
					if(ce.isArrive(day, month+1, year)){
						textAreaEvent.append("EVENTNAME : "+ce.getEventName()+"\nDESCRIPTION : "+ce.getEventDescription()+"\nDATE"+ce.deadLineToString()+"\n\n");
					}
				}
			}
		});

	}

	@Override
	public void run() {
		this.setVisible(true);
	}
}