package UI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import Homework.Homework;

/**
 * Label of homework to add action when push on homework list UI.
 * @author Pongsakorn somsri
 *
 */
public class HomeworkLabel extends JLabel{
	
	private Homework hw;
	
	/**
	 * Constructor for set UI.
	 * @param hw that used for create label.
	 */
	public HomeworkLabel(Homework hw){
		this.hw = hw;
		this.setOpaque(true);
		this.setBackground(Color.GRAY);
		this.setPreferredSize(new Dimension(350,30));
		this.initComponent();
		this.setText(String.format("NAME:%s      DEADLINE:%s    ClickToDescription",hw.getHomeworkName(),hw.deadLineToString()));
		this.setHorizontalAlignment(SwingConstants.CENTER);
		
	}
	
	/**
	 * To add action with label.
	 */
	public void initComponent(){
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				HomeworkDetailUI hd = new HomeworkDetailUI(hw);
				hd.run();
			}
		});
	}

}
