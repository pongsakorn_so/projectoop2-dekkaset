package UI;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import User.User;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * User Interface to show detail of user.
 * @author Parisa Supitayakul
 *
 */
public class ProfileUI extends JFrame implements Runnable{
	private User user;
	private JPanel contentPane;
	private JFrame frames;
	private JTextArea textArea;
	private JButton btnOK;
	private JLabel lblId,lblUserid,lblName,lblLastname,lblEmail;
	private String fileURL;
	private URL urlImg;
	private JPanel panel;
	private JLabel lblNewLabel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					User user = new User("57001","momo","123","Paris","mingming","mo@gmail.com");
					ProfileUI frame = new ProfileUI(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public ProfileUI(User user) {
		setResizable(false);
		setTitle("Profile");
		this.user = user;
	
		//setUndecorated(true);
		setBounds(300, 300, 500, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		frames = new JFrame();
		contentPane.setLayout(null);
		lblId = new JLabel("ID :");
		lblId.setBounds(42, 80, 46, 14);
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contentPane.add(lblId);
		lblName = new JLabel("Name :");
		lblName.setBounds(42, 110, 60, 14);
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contentPane.add(lblName);
		lblLastname = new JLabel("Lastname :");
		lblLastname.setBounds(42, 140, 85, 14);
		lblLastname.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contentPane.add(lblLastname);
		lblEmail = new JLabel("E-mail :");
		lblEmail.setBounds(42, 170, 85, 15);
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contentPane.add(lblEmail);
		
		btnOK = new JButton("Close");
		btnOK.setBounds(201, 227, 89, 23);
		contentPane.add(btnOK);
		lblUserid = new JLabel("USER_ID");
		lblUserid.setBounds(125, 80, 137, 15);
		lblUserid.setForeground(Color.GRAY);
		lblUserid.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblUserid);
		JLabel lblName_1 = new JLabel("Name");
		lblName_1.setBounds(125, 110, 117, 15);
		lblName_1.setForeground(Color.GRAY);
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblName_1);
		JLabel lblLastname_1 = new JLabel("Lastname");
		lblLastname_1.setBounds(125, 140, 89, 15);
		lblLastname_1.setForeground(Color.GRAY);
		lblLastname_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblLastname_1);
		JLabel lblEmail_1 = new JLabel("email");
		lblEmail_1.setBounds(125, 170, 117, 15);
		lblEmail_1.setForeground(Color.GRAY);
		lblEmail_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblEmail_1);
		lblUserid.setText(user.getUserID());
		lblName_1.setText(user.getFirstname());
		lblLastname_1.setText(user.getLastname());
		lblEmail_1.setText(user.getEmail());
		
		panel = new JPanel();
		panel.setBounds(0, 0, 494, 50);
		contentPane.add(panel);
		panel.setLayout(null);
		
		lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(ProfileUI.class.getResource("/images/testbg.jpg")));
		lblNewLabel.setBounds(0, 0, 494, 50);
		panel.add(lblNewLabel);
		btnOK.addActionListener(new OKListener());
	}
	class OKListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			ProfileUI.this.setVisible(false);
		}
	}
	@Override
	public void run() {
		this.setVisible(true);
	}
}