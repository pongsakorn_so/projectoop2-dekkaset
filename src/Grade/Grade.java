package Grade;
/**
 * Enum of grade including String of grade and double grade.
 * @author Parisa Supitayakul
 *
 */
public enum Grade {
	A("A",4),
    BPLUS("B+",3.5),
    B("B",3),
    CPLUS("C+",2.5),
    C("C",2),
    DPLUS("D+",1.5),
    D("D",1),
	F("F",7);
    
    
    public final String gradeName;
    public final double gradeValue;
    /**
     * Constructor for declare vaiable.
     * @param gradeName is a name of each grade.
     * @param gradeValue is a value of each grade.
     */
    Grade(String gradeName,double gradeValue){
        this.gradeName = gradeName;
        this.gradeValue = gradeValue;
    }

    /**
     * To get grade name.
     * @return grade name.
     */
    public String getGradeName(){
        return gradeName;
    }
    
    /**
     * To get grade value.
     * @return grade value.
     */
    public double getGradeValue(){
        return gradeValue;
    }
    
    /**
     * To get string  of grade name . 
     */
    public String toString(){
    	return this.gradeName;
    }
    
}
