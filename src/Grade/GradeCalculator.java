package Grade;
import java.util.ArrayList;
import java.util.List;

import Course.Course;

/**
 * To calculate grade by using course and grade of each course.
 * @author Parisa Supitayakul.
 *
 */
public class GradeCalculator {

	public GradeCalculator(){

	}
	/**
	 * To calculate grade using credit multiply by grade of each course 
	 * and sum all grade and  all of credit.
	 * then use sum all grade divide by all of credit.
	 * @param courseList is a list of course that student enroll.
	 * @param gradeList ,guess value that you expected.
	 * @return grade that your expected in double value.
	 */
	public double calculateGrade(List<Course> courseList,List<Grade> gradeList){
		double sumOfGrade=0;
		double allCredit=0;
		
		for(Grade grade : gradeList)
			System.out.println(grade.getGradeValue());
		for(int i=0;i<courseList.size();i++){
			Course course = courseList.get(i);
			Grade grade = gradeList.get(i);
			if(course!=null&&grade!=null){
			sumOfGrade += course.getCourseCredit()*grade.getGradeValue();
			allCredit += course.getCourseCredit();
			}
		}
		
		return sumOfGrade/allCredit;
	}
	
}
