package DigitalClock;
import java.util.Date;
import java.util.Observable;


public class SuperCheapClock extends Observable{
	private static SuperCheapClock clock;
	private Date currentTime;
	
	private SuperCheapClock(){
		currentTime = new Date();
	}
	
	public static SuperCheapClock getInstance(){
		if(clock==null)
			clock = new SuperCheapClock();
		return clock;
	}
	
	public Date getDate(){
		return currentTime;
	}
	
	public Date getCurrentTime(){
		return currentTime;
	}
	
	public void updateTime(){
		this.notifyObservers();
		this.setChanged();
		currentTime.setTime(System.currentTimeMillis());
	}
	
	public String dateToString(){
		return String.format("%s / %s / %s",this.currentTime.getDate(),this.currentTime.getMonth()+1,this.currentTime.getYear()+1900);
	}
}
