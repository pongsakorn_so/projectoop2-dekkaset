package DigitalClock;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class DigitalClockPanel extends JPanel{
	
	private final int delayForUpdate = 100;
	ActionListener taskUpdateTimer = new ActionListener() {
		public void actionPerformed(ActionEvent event){ 
			updateTime();
		}
	};
	javax.swing.Timer updateTimer = new javax.swing.Timer(delayForUpdate, taskUpdateTimer);

	
	private SuperCheapClock digitalClock;
	private ClassLoader classLoader = this.getClass().getClassLoader();
	private JLabel hourDigit1,hourDigit2,minuteDigit1,minuteDigit2,secondDigit1,secondDigit2,colon1,colon2,date;
	private Map<Integer,ImageIcon> numDisplay = new HashMap<Integer,ImageIcon>();
	private int hourD1,hourD2,minuteD1,minuteD2,secondD1,secondD2;
	
	
	public DigitalClockPanel(SuperCheapClock digitalClock){
		this.digitalClock = digitalClock;
		this.putImageToMap();
		this.initComponent();
	}
	
	public void initComponent(){
		/*setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);*/
		updateTimer.start();
		this.setPreferredSize(new Dimension(400,100));
		super.setBorder(new EmptyBorder(0,0,0,0));
		
		//this = new JPanel();
		Container timeContainer = new Container();
		Container dateContainer = new Container();
		hourDigit1 = new JLabel();
		hourDigit2 = new JLabel();
		minuteDigit1 = new JLabel();
		minuteDigit2 = new JLabel();
		secondDigit1 = new JLabel();
		secondDigit2 = new JLabel();
		colon1 = new JLabel();
		colon2 = new JLabel();
		date = new JLabel();
		
		this.setBackground(Color.black);

		hourDigit1.setIcon(numDisplay.get(1));
		hourDigit2.setIcon(numDisplay.get(1));
		minuteDigit1.setIcon(numDisplay.get(1));
		minuteDigit2.setIcon(numDisplay.get(1));
		secondDigit1.setIcon(numDisplay.get(1));
		secondDigit2.setIcon(numDisplay.get(1));
		colon1.setIcon(numDisplay.get(10));
		colon2.setIcon(numDisplay.get(10));
		date.setText(digitalClock.dateToString());


		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		date.setForeground(Color.orange);
		timeContainer.setLayout(new FlowLayout());
		dateContainer.setLayout(new FlowLayout());

		//super.setTitle("Cheap Digital Clock by Pongsakorn Somsri");
		//super.add(this);
		this.add(timeContainer);
		this.add(dateContainer);

		timeContainer.add(hourDigit1);
		timeContainer.add(hourDigit2);
		timeContainer.add(colon1);
		timeContainer.add(minuteDigit1);
		timeContainer.add(minuteDigit2);
		timeContainer.add(colon2);
		timeContainer.add(secondDigit1);
		timeContainer.add(secondDigit2);
		
		dateContainer.add(date);
		
		
		
	}
	
	public void putImageToMap(){
		for (int i = -1 ; i <= 10; i++){
			URL image =  classLoader.getResource("images/"+i+".gif");
			ImageIcon num = new ImageIcon(image);
			numDisplay.put(i,num);
		}
	}
	
	public void updateTime(){
		digitalClock.updateTime();
		hourD1 = digitalClock.getCurrentTime().getHours()/10;
		hourD2 = digitalClock.getCurrentTime().getHours()%10;
		minuteD1 = digitalClock.getCurrentTime().getMinutes()/10;
		minuteD2 = digitalClock.getCurrentTime().getMinutes()%10;
		secondD1 = digitalClock.getCurrentTime().getSeconds()/10;
		secondD2 = digitalClock.getCurrentTime().getSeconds()%10;

		hourDigit1.setIcon(numDisplay.get(hourD1));
		hourDigit2.setIcon(numDisplay.get(hourD2));
		minuteDigit1.setIcon(numDisplay.get(minuteD1));
		minuteDigit2.setIcon(numDisplay.get(minuteD2));
		secondDigit1.setIcon(numDisplay.get(secondD1));
		secondDigit2.setIcon(numDisplay.get(secondD2));
		date.setText(digitalClock.dateToString());

	}
	
	public void setBgColor(Color color){
		this.setBackground(color);
		//super.setBackground(color);
	}
	
}
