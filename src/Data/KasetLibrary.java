package Data;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import Calendar.CalendarEvent;
import Course.Course;
import Homework.Homework;
/**
 * Class that use to store the data that input from the database;
 * @author Pongsakorn Somsri
 *
 */
public class KasetLibrary {
	private static KasetLibrary library;
	private List<Course> allCourse;
	private List<CalendarEvent> allCalendarEvent;
	private Database database;

	private KasetLibrary(){
		database = Database.getInstance();
		allCourse = new ArrayList<Course>();
		allCalendarEvent = new ArrayList<CalendarEvent>();
	}

	/**
	 * Singleton of this class
	 */
	public static KasetLibrary getInstance() {
		if(library==null){
			library = new KasetLibrary();
		}
		return library;
	}

	/**
	 * Check that library has this course or not.
	 * @param courseID that want to check to the library.
	 * @return true if there has this course in library otherwise false.
	 */
	public boolean hasThisCourse(String courseID){
		for(Course subs: allCourse){
			if(subs.getCourseID().equalsIgnoreCase(courseID)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Updata the library by input the value from the database.
	 */
	public void updateLibrary(){
		this.allCourse = database.getCourseList();
		for(Course course : allCourse){
			database.addCourseTimeToCourse(course);
			database.addHomeworkListToCourse(course);
		}
		this.allCalendarEvent = database.getEventList();
	}

	/**
	 * Method use to add Course to the library.
	 * @param course that want to add.
	 * @return true if can add.otherwise false.
	 */
	public boolean addCourseToDB(Course course){
		if(!this.hasThisCourse(course.getCourseID())){
			allCourse.add(course);
			return true;
		}
		return false;
	}

	/**
	 * Method use to remove course from library that courseID equal the input.
	 * @param courseID of course that want to remove.
	 * @return true if can remove.otherwise false.
	 */
	public boolean removeCourseFromDB(String courseID){
		for(Course subs: allCourse){
			if(subs.getCourseID().equalsIgnoreCase(courseID))
				allCourse.remove(subs);
			return true;
		}

		return false;
	}

	public List<Course> getAllCourse(){
		return this.allCourse;
	}

	/**
	 * Get course from the following id.
	 * @param id is course id.
	 * @return courseb of the following course id.
	 */
	public Course getCourse(String id){
		for(Course course : allCourse){
			if(course.getCourseID().equalsIgnoreCase(id)){
				return course;
			}
		}
		return null;
	}

	public List<CalendarEvent> getCalendarList(){
		return this.allCalendarEvent;
	}

	/**
	 * get all homework from the library.
	 * @return list of homework.
	 */
	public List<Homework> getAllHomework(){
		List<Course> allCourse = this.getAllCourse();
		List<Homework> allHomeworkList = new ArrayList<Homework>();
		for(Course course : allCourse){
			List<Homework> courseHomeworkList = course.getHomeworkList();
			for(Homework homework : courseHomeworkList)
				allHomeworkList.add(homework);
		}
		return allHomeworkList;
	}

	public void printAllCourse(){
		for(Course course : allCourse)
			System.out.println(course.toString());
	}

	public void printAllCalendarList(){
		for(CalendarEvent event : allCalendarEvent){
			System.out.println(event.toString());
		}
	}

	/**
	 * remove all course from the library.
	 */
	public void removeAllCourse(){
		allCourse.clear();
	}

	/**
	 * remove all calendar event from the list.
	 */
	public void removeAllCalendarEvent(){
		allCalendarEvent.clear();
	}
}
