package Data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import Calendar.CalendarEvent;
import Course.Course;
import Course.Day;
import Homework.Homework;
import User.User;

/**
 * A database class that use a sqlite to connect to the database and get or edit the database using sql statement.
 * @author Pongsakorn Somsri.
 *
 */
public class Database {

	private static Database database;
	
	private Connection c;
	private Statement stmt;

	private Database(){

	}

	/**
	 * singleton of database.
	 * @return database.s
	 */
	public static Database getInstance(){
		if(database == null){
			database = new Database();
		}

		return database;
	}

	/**
	 * get all course list from database.
	 * @return list of course from database.
	 */
	public List<Course> getCourseList(){
		List<Course> allCourse = new ArrayList<Course>();
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Course.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM COURSE;" );
			while ( rs.next() ) {
				String code = rs.getString("COURSEID");
				String name = rs.getString("NAME");
				String des  = rs.getString("DESCRIPTION");
				int credit = rs.getInt("COURSECREDIT");
				Course newCourse = new Course(code,name,des,credit);
				allCourse.add(newCourse);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		return allCourse;
	}

	/**
	 * get all userList from the database.
	 * @return list of user.
	 */
	public List<User> getUserList(){
		List<User> allUser = new ArrayList<User>();
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM USER;" );
			while ( rs.next() ) {
				String id = rs.getString("ID");
				String username = rs.getString("USERNAME");
				String password  = rs.getString("PASSWORD");
				String fName = rs.getString("FIRSTNAME");
				String lName = rs.getString("LASTNAME");
				String email = rs.getString("EMAIL");
				User newUser = new User(id,username,password,fName,lName,email);
				allUser.add(newUser);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		return allUser;
	}

	/**
	 * get all homework from the database.
	 * @return list of homework.
	 */
	public List<Homework> getHomeworkList(){
		List<Homework> allHomework = new ArrayList<Homework>();
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Course.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM HOMEWORK;" );
			while ( rs.next() ) {
				String courseId = rs.getString("ID");
				String name = rs.getString("NAME");
				String description  = rs.getString("DESCRIPTION");
				String deadLine = rs.getString("DEADLINE");
				Homework newHomework = new Homework(courseId,name,description,deadLine);
				allHomework.add(newHomework);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		return allHomework;
	}

	/**
	 * get all event from the database
	 * @return list of event.
	 */
	public List<CalendarEvent> getEventList(){
		List<CalendarEvent> allEvent = new ArrayList<CalendarEvent>();
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Event.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM EVENT;" );
			while ( rs.next() ) {

				String name = rs.getString("NAME");
				String description  = rs.getString("DESCRIPTION");
				String date = rs.getString("DATE");
				CalendarEvent newEvent = new CalendarEvent(name,description,date);
				allEvent.add(newEvent);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		return allEvent;
	}

	/**
	 * get the enroll list of the following user.
	 * @param user is the owner of this enrolllist.
	 * @return enrolllist of user.
	 */
	public List<String> getAllUserEnrollList(User user){

		List<String> enrollList = new ArrayList<String>();
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM ENROLLLIST WHERE USERID="+user.getUserID()+";" );
			System.out.println(user.getUserID());
			while ( rs.next() ) {
				String courseID  = rs.getString("COURSEID");
				enrollList.add(courseID);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		return enrollList;
	}

	/**
	 * add enroll to the input student.
	 * @param user that want to add enrolllist.
	 */
	public void addEnrollListToUser(User user){
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM ENROLLLIST WHERE USERID="+user.getUserID()+";" );
			System.out.println(user.getUserID());
			while ( rs.next() ) {
				String courseID  = rs.getString("COURSEID");
				System.out.println(user.addCourse(courseID));
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	/**
	 * Get a homework form database and add it into course.
	 * @param course course that want to add the homework.
	 */
	public void addHomeworkListToCourse(Course course){

		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Course.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM HOMEWORK WHERE COURSEID="+course.getCourseID()+";" );
			while ( rs.next() ) {
				String name = rs.getString("NAME");
				String description  = rs.getString("DESCRIPTION");
				String deadLine = rs.getString("DEADLINE");
				course.addHomework(name, description, deadLine);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	/**
	 * Method that use to add course time to input course.
	 * @param course that want to add coursetime.
	 */
	public void addCourseTimeToCourse(Course course){
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Course.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM COURSETIME WHERE COURSEID="+course.getCourseID()+";" );
			while ( rs.next() ) {
				Day day = Day.valueOf(rs.getString("DAY").toUpperCase());
				String startTime  = rs.getString("STARTTIME");
				String endTime = rs.getString("ENDTIME");
				course.addCourseTime(day, startTime, endTime);
				System.out.println("addCourseTime");
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	/**
	 * method use to insert a enroll list to the enrolllist table.
	 */
	public void addEnrollToUserInDB(String userID,String courseID){
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			String sql = "INSERT INTO ENROLLLIST (USERID,COURSEID) " +
					"VALUES ('"+userID+"','"+courseID+"');"; 
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Records created successfully");
	}

	/**
	 * remove enrolllist of the input user id with this course id.
	 * @param userID
	 * @param courseID
	 */
	public void removeEnrollFromUserInDB(String userID,String courseID){
		c = null;
		stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			
			System.out.println(userID+"       "+courseID);
			stmt = c.createStatement();
			String sql = "DELETE from ENROLLLIST where USERID='"+userID+"' AND COURSEID='"+courseID+"';";
			stmt.executeUpdate(sql);
			c.commit();
			System.out.println("DELETE");

			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
	
	/**
	 * a method that use to get the user from the inputUsername and input password.
	 * @return user if has the user that has the following username and password.
	 */
	public User getUserLogin(String inUsername,String inPassword){
		c = null;
		stmt = null;
		User user = null;
		
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM USER WHERE USERNAME='"+inUsername+"' AND PASSWORD='"+inPassword+"';" );
			while ( rs.next() ) {
				String id = rs.getString("ID");
				String username = rs.getString("USERNAME");
				String password = rs.getString("PASSWORD");
				String fName = rs.getString("FIRSTNAME");
				String lName = rs.getString("LASTNAME");
				String email = rs.getString("EMAIL");
				user = new User(id,username,password,fName,lName,email);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		
		return user;
	}
	
	/**
	 * Method user to check that this username is same from username database or not.
	 * @param username that want to check.
	 * @return true if it duplicate name.
	 */
	public boolean isDuplicateUsername(String username){
		c = null;
		stmt = null;
		boolean dup=false;
		
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			 

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM USER WHERE USERNAME='"+username+"';" );
			while ( rs.next() ) {
				dup = true;
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		
		return dup;
	}
	
	/**
	 * Method use to add the data of person to the database.
	 */
	public void addUserToDB(String username,String password,String firstName,String lastName,String email){
		c = null;
		stmt = null;
		try {
			int currentID = 0;
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:User.db");
			c.setAutoCommit(false);
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("select count(*) from USER");
			if(rs.next())
				currentID = rs.getInt(1);
			
			String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD,FIRSTNAME,LASTNAME,EMAIL) " +
					"VALUES ('"+(currentID+1)+"','"+username+"','"+password+"','"+firstName+"','"+lastName+"','"+email+"');"; 
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Records created successfully");
	}




}
