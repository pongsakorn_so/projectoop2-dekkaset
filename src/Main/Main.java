package Main;

import java.awt.EventQueue;
import java.util.function.Predicate;

import UI.LoginUI;


public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginUI login = new LoginUI();
					login.run();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
}
